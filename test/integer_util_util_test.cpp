#include "big/detail/part_type.h"
#include "big/detail/util.h"

#include <gtest/gtest.h>

#include <bitset>
#include <climits>
#include <random>
#include <vector>

using namespace big::detail;

TEST(IntegerImplUtil, AddOverflow) {
    {
        const part_type a = 0;
        const part_type b = 0;
        part_type result;
        const part_type overflow = add_overflow(a, b, &result);
        const part_type expected_result = 0;
        const part_type expected_overflow = 0;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        const part_type a = negative_one;
        const part_type b = 1;
        part_type result;
        const part_type overflow = add_overflow(a, b, &result);
        const part_type expected_result = 0;
        const part_type expected_overflow = 1;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        const part_type a = negative_two;
        const part_type b = 1;
        part_type result;
        const part_type overflow = add_overflow(a, b, &result);
        const part_type expected_result = negative_one;
        const part_type expected_overflow = 0;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        const part_type a = 115;
        const part_type b = 12;
        part_type result;
        const part_type overflow = add_overflow(a, b, &result);
        const part_type expected_result = 115 + 12;
        const part_type expected_overflow = 0;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        const part_type a = negative_one;
        const part_type b = negative_one;
        part_type result;
        const part_type overflow = add_overflow(a, b, &result);
        const part_type expected_result = negative_two;
        const part_type expected_overflow = 1;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
}

TEST(IntegerImplUtil, PlusEqualsWithOverflow) {
    {
        part_type a = 0;
        const part_type b = 0;
        const part_type overflow = plus_equals_with_overflow(&a, b);
        const part_type expected_result = 0;
        const part_type expected_overflow = 0;
        ASSERT_EQ(a, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        part_type a = negative_one;
        const part_type b = 1;
        const part_type overflow = plus_equals_with_overflow(&a, b);
        const part_type expected_result = 0;
        const part_type expected_overflow = 1;
        ASSERT_EQ(a, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        part_type a = negative_two;
        const part_type b = 1;
        const part_type overflow = plus_equals_with_overflow(&a, b);
        const part_type expected_result = negative_one;
        const part_type expected_overflow = 0;
        ASSERT_EQ(a, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        part_type a = 115;
        const part_type b = 12;
        const part_type overflow = plus_equals_with_overflow(&a, b);
        const part_type expected_result = 115 + 12;
        const part_type expected_overflow = 0;
        ASSERT_EQ(a, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
}

TEST(IntegerImplUtil, SubtractOverflow) {
    {
        const part_type a = 0;
        const part_type b = 0;
        part_type result;
        const part_type overflow = subtract_overflow(a, b, &result);
        const part_type expected_result = 0;
        const part_type expected_overflow = 0;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        const part_type a = 0;
        const part_type b = 1;
        part_type result;
        const part_type overflow = subtract_overflow(a, b, &result);
        const part_type expected_result = negative_one;
        const part_type expected_overflow = 1;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        const part_type a = negative_one;
        const part_type b = negative_one;
        part_type result;
        const part_type overflow = subtract_overflow(a, b, &result);
        const part_type expected_result = 0;
        const part_type expected_overflow = 0;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        const part_type a = negative_one;
        const part_type b = negative_two;
        part_type result;
        const part_type overflow = subtract_overflow(a, b, &result);
        const part_type expected_result = 1;
        const part_type expected_overflow = 0;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        const part_type a = negative_two;
        const part_type b = negative_one;
        part_type result;
        const part_type overflow = subtract_overflow(a, b, &result);
        const part_type expected_result = negative_one;
        const part_type expected_overflow = 1;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
    {
        const part_type a = most_negative_value;
        const part_type b = 1;
        part_type result;
        const part_type overflow = subtract_overflow(a, b, &result);
        const part_type expected_result = most_positive_value;
        const part_type expected_overflow = 0;
        ASSERT_EQ(result, expected_result);
        ASSERT_EQ(overflow, expected_overflow);
    }
}

TEST(IntegerImplUtil, CheckIfTwoMostSignificantBitsAreTheSamePortable) {
    std::vector<part_type> candidates;

    // add specific candidates:
    candidates.push_back(0);
    candidates.push_back(negative_one);
    candidates.push_back(negative_two);
    candidates.push_back(most_positive_value);
    candidates.push_back(most_negative_value);

    // add random candidates:
    std::mt19937 generator;
    std::uniform_int_distribution<part_type> distribution(0, negative_one);
    auto random_part = [&] { return distribution(generator); };
    for (int i = 0; i < 1000; i++) {
        candidates.push_back(random_part());
    }

    // test all candidates:
    for (part_type candidate : candidates) {
        std::bitset<part_type_width> intermediate = candidate;
        auto most_significant_bit = intermediate[part_type_width - 1];
        auto second_most_significant_bit = intermediate[part_type_width - 2];

        most_significant_bit = 0;
        second_most_significant_bit = 0;
        part_type adjusted_candidate = intermediate.to_ullong();
        bool result = check_if_two_most_significant_bits_are_the_same_portable(adjusted_candidate);
        ASSERT_TRUE(result);

        most_significant_bit = 1;
        second_most_significant_bit = 1;
        adjusted_candidate = intermediate.to_ullong();
        result = check_if_two_most_significant_bits_are_the_same_portable(adjusted_candidate);
        ASSERT_TRUE(result);

        most_significant_bit = 0;
        second_most_significant_bit = 1;
        adjusted_candidate = intermediate.to_ullong();
        result = check_if_two_most_significant_bits_are_the_same_portable(adjusted_candidate);
        ASSERT_FALSE(result);

        most_significant_bit = 1;
        second_most_significant_bit = 0;
        adjusted_candidate = intermediate.to_ullong();
        result = check_if_two_most_significant_bits_are_the_same_portable(adjusted_candidate);
        ASSERT_FALSE(result);
    }
}

#if defined(__GNUC__) && defined(__x86_64__)
TEST(IntegerImplUtil, CheckIfTwoMostSignificantBitsAreTheSameX64Gcc) {
    std::vector<uint64_t> candidates;

    // add specific candidates:
    candidates.push_back(0);
    candidates.push_back(negative_one);
    candidates.push_back(negative_two);
    candidates.push_back(most_positive_value);
    candidates.push_back(most_negative_value);

    // add random candidates:
    std::mt19937 generator;
    std::uniform_int_distribution<uint64_t> distribution(0, negative_one);
    auto random_part = [&] { return distribution(generator); };
    for (int i = 0; i < 1000; i++) {
        candidates.push_back(random_part());
    }

    // test all candidates:
    for (uint64_t candidate : candidates) {
        std::bitset<64> intermediate = candidate;
        auto most_significant_bit = intermediate[64 - 1];
        auto second_most_significant_bit = intermediate[64 - 2];

        most_significant_bit = 0;
        second_most_significant_bit = 0;
        uint64_t adjusted_candidate = intermediate.to_ullong();
        bool result = check_if_two_most_significant_bits_are_the_same_x86_64_gcc(adjusted_candidate);
        ASSERT_TRUE(result);

        most_significant_bit = 1;
        second_most_significant_bit = 1;
        adjusted_candidate = intermediate.to_ullong();
        result = check_if_two_most_significant_bits_are_the_same_x86_64_gcc(adjusted_candidate);
        ASSERT_TRUE(result);

        most_significant_bit = 0;
        second_most_significant_bit = 1;
        adjusted_candidate = intermediate.to_ullong();
        result = check_if_two_most_significant_bits_are_the_same_x86_64_gcc(adjusted_candidate);
        ASSERT_FALSE(result);

        most_significant_bit = 1;
        second_most_significant_bit = 0;
        adjusted_candidate = intermediate.to_ullong();
        result = check_if_two_most_significant_bits_are_the_same_x86_64_gcc(adjusted_candidate);
        ASSERT_FALSE(result);
    }
}
#endif
