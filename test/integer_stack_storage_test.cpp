#include "big/detail/integer_stack_storage.h"

#include "big/detail/memory.h"
#include "big/detail/part_type.h"

#include <gtest/gtest.h>

using big::detail::integer_stack_storage;

class IntegerStackStorage : public ::testing::Test {
    std::vector<big::detail::part_type> valid_integral_values;
    std::vector<void*> valid_pointers;
};

TEST(IntegerStackStorage, StoreNullPointer) {
    integer_stack_storage a;
    void* b = nullptr;
    a.store_pointer(b);
    void* c = a.load_pointer();
    EXPECT_EQ(b, c);
    EXPECT_TRUE(a.holds_pointer());
}

TEST(IntegerStackStorage, StoreActualAlignedPointer) {
    integer_stack_storage a;
    void* b = big::detail::aligned_alloc_2(100);
    a.store_pointer(b);
    void* c = a.load_pointer();
    EXPECT_EQ(b, c);
    EXPECT_TRUE(a.holds_pointer());
    big::detail::aligned_free_2(b);
}

TEST(IntegerStackStorage, StoreSixteen) {
    integer_stack_storage a;
    big::detail::part_type b = 16;
    a.store_integral(b);
    big::detail::part_type c = a.load_integral();
    EXPECT_EQ(b, c);
    EXPECT_TRUE(a.holds_integral());
}
