#include <big/detail/view.h>
#include <big/integer.h>

#include <gtest/gtest-typed-test.h>
#include <gtest/gtest.h>

#include <map>
#include <sstream>
#include <string>
#include <tuple>

TEST(IntegerTest, ConstructFromStringPrimitive) {
    std::map<const char*, const char*> c = {{"100", "100"}, {"015", "15"}, {"-01", "-1"}, {"0", "0"}, {"000", "0"}, {"-010000000000000000000000000", "-10000000000000000000000000"}};

    for (std::tuple<const char*, const char*> x : c) {
        const char* from = std::get<0>(x);
        const char* expected = std::get<1>(x);
        big::integer number = from;
        std::stringstream ss;
        ss << number;
        ASSERT_EQ(ss.str(), expected);
    }
}

// TODO(ben.hansell): add construct from literal test & functionality

TEST(IntegerTest, ConstructFromStringObject) {

    std::map<const char*, const char*> c = {{"100", "100"}, {"015", "15"}, {"-01", "-1"}, {"0", "0"}, {"000", "0"}, {"-010000000000000000000000000", "-10000000000000000000000000"}};

    for (std::tuple<const char*, const char*> x : c) {
        std::string from = std::get<0>(x);
        std::string expected = std::get<1>(x);
        big::integer number = from;
        std::stringstream ss;
        ss << number;
        ASSERT_EQ(ss.str(), expected);
    }
}

TEST(IntegerTest, ConstructFromLiteral) {
    using namespace big::literals;

    auto small_value = 10_bi;
    auto same_small_value = 4_bi + 6_bi;
    ASSERT_EQ(small_value, same_small_value);

    auto big_value = 100000000000000000000000000000000000000000_bi;
    auto same_big_value = 100000000000000000000000000000000000000006_bi - 6_bi;
    ASSERT_EQ(big_value, same_big_value);
}

TEST(IntegerTest, ConstructFromNegativeLiteral) {
    using namespace big::literals;

    auto small_value = -10_bi;
    auto same_small_value = -4_bi - 6_bi;
    ASSERT_EQ(small_value, same_small_value);

    auto big_value = -100000000000000000000000000000000000000000_bi;
    auto same_big_value = -100000000000000000000000000000000000000006_bi + 6_bi;
    ASSERT_EQ(big_value, same_big_value);
}

TEST(IntegerTest, Add) {
    big::integer a = "100";
    big::integer b = "6000000000000000000000000000000000000";
    std::stringstream ss;
    ss << (a + b);
    ASSERT_EQ(ss.str(), "6000000000000000000000000000000000100");
}

TEST(IntegerTest, Subtract) {
    big::integer a = "100";
    big::integer b = "6000000000000000000000000000000000100";
    std::stringstream ss;
    ss << (a - b);
    ASSERT_EQ(ss.str(), "-6000000000000000000000000000000000000");
}

TEST(IntegerTest, Multiply) {
    big::integer a = "-200";
    big::integer b = "4000000000000000000000000000000000100";
    std::stringstream ss;
    ss << (a * b);
    ASSERT_EQ(ss.str(), "-800000000000000000000000000000000020000");
}

TEST(IntegerTest, Divide) {
    big::integer a = "40000000000000000000000000000000001000";
    big::integer b = "-200";
    std::stringstream ss;
    ss << (a / b);
    ASSERT_EQ(ss.str(), "-200000000000000000000000000000000005");
}

TEST(IntegerTest, DivideAndRound) {
    big::integer a = "100";
    big::integer b = "6";
    std::stringstream ss;
    ss << (a / b);
    ASSERT_EQ(ss.str(), "16");
}

TEST(IntegerTest, DivideAndRoundNegative) {
    big::integer a = "-100";
    big::integer b = "6";
    std::stringstream ss;
    ss << (a / b);
    ASSERT_EQ(ss.str(), "-16");
}

TEST(IntegerTest, Modulus) {
    big::integer a = "100";
    big::integer b = "6";
    std::stringstream ss;
    ss << (a % b);
    ASSERT_EQ(ss.str(), "4");
}

TEST(IntegerTest, ModulusNegativeNumerator) {
    big::integer a = "-100";
    big::integer b = "6";
    std::stringstream ss;
    ss << (a % b);
    ASSERT_EQ(ss.str(), "-4");
}

TEST(IntegerTest, ModulusNegativeDenominator) {
    big::integer a = "100";
    big::integer b = "-6";
    std::stringstream ss;
    ss << (a % b);
    ASSERT_EQ(ss.str(), "4");
}

TEST(IntegerTest, ModulusNegativeNumeratorAndDenominator) {
    big::integer a = "-100";
    big::integer b = "-6";
    std::stringstream ss;
    ss << (a % b);
    ASSERT_EQ(ss.str(), "-4");
}

TEST(IntegerTest, Equals) {
    {
        big::integer a = "-100";
        big::integer b = "-6";
        ASSERT_NE(a, b);
    }
    {
        big::integer a = "-100";
        big::integer b = "-100";
        ASSERT_EQ(a, b);
    }
    {
        big::integer a = "64882554382543485342328253235323238532318532246836687931971798545237452845180001465153213";
        big::integer b = "64882554382543485342328253235323238532318532246836687931971798545237452845180001465153213";
        ASSERT_EQ(a, b);
    }
    {
        big::integer a = "64882554382543485342328253235323238542318532246836687931971798545237452845180001465153213";
        big::integer b = "64882554382543485342328253235323238532318532246836687931971798545237452845180001465153213";
        ASSERT_NE(a, b);
    }
}

TEST(IntegerTest, UnaryMinus) {
    big::integer a = "100";
    big::integer b = -a;
    std::stringstream ss;
    ss << b;
    ASSERT_EQ(ss.str(), "-100");
}

TEST(IntegerTest, Copy) {
    big::integer a = "-100";
    big::integer b = "10000000000000000000000000000000000000000000000000000000";
    big::integer c = a;
    b = a;
    ASSERT_EQ(a, b);
    ASSERT_EQ(b, c);
}

TEST(IntegerTest, Move) {
    big::integer a = "-100";
    big::integer b = "10000000000000000000000000000000000000000000000000000000";
    big::integer c = std::move(a);
    b = std::move(c);
    big::integer d = "-100";
    a = c = d;
    ASSERT_EQ(a, b);
    ASSERT_EQ(b, c);
    ASSERT_EQ(c, d);
}

#pragma GCC diagnostic push // TODO(ben.hansell): ensure this doesn't fail to compile with MSVC, and that any equivalent warnings are suppressed
#pragma GCC diagnostic ignored "-Wself-assign-overloaded"
#pragma GCC diagnostic ignored "-Wself-move"
TEST(IntegerTest, SelfAssignmentSmallValue) {
    big::integer a = "-100";
    big::integer b = "-100";
    a = a;
    ASSERT_EQ(a, b);
    a = std::move(a);
    ASSERT_EQ(a, b);
}

TEST(IntegerTest, SelfAssignmentLargeValue) {
    big::integer a = "-700000000000000000000000000000000000000000000000000000000000000000";
    big::integer b = "-700000000000000000000000000000000000000000000000000000000000000000";
    a = a;
    ASSERT_EQ(a, b);
    a = std::move(a);
    ASSERT_EQ(a, b); 
}
#pragma GCC diagnostic pop