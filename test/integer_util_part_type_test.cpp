#include "big/detail/part_type.h"

#include <gtest/gtest.h>

using namespace big::detail;

TEST(IntegerImplPartType, DualPartTypeAdd) {
    {
        dual_part_type a(0, 0);
        dual_part_type b = a + a;
        EXPECT_EQ(a, b);
    }
    {
        dual_part_type a(negative_one, 0);
        dual_part_type b(1, 0);
        dual_part_type sum = a + b;
        dual_part_type expected(0, 1);
        EXPECT_EQ(sum, expected);
    }
    {
        dual_part_type a(negative_one, 0);
        dual_part_type b(1, 16);
        dual_part_type sum = a + b;
        dual_part_type expected(0, 17);
        EXPECT_EQ(sum, expected);
    }
}

TEST(IntegerImplPartType, DualPartTypeShiftLeft) {
    {
        dual_part_type original(1, 1);
        dual_part_type result = original << (part_type_width - 1);
        dual_part_type expected(part_type(1) << (part_type_width - 1), part_type(1) << (part_type_width - 1));
        EXPECT_EQ(result, expected);
    }
    {
        dual_part_type original(negative_one, negative_one);
        dual_part_type result = original << 1;
        dual_part_type expected(negative_one << 1u, negative_one);
        EXPECT_EQ(result, expected);
    }
    {
        dual_part_type original(negative_one, negative_one);
        dual_part_type result = original << (part_type_width - 1);
        dual_part_type expected(most_negative_value, negative_one);
        EXPECT_EQ(result, expected);
    }
    {
        dual_part_type original(negative_one, negative_one);
        dual_part_type result = original << (part_type_width);
        dual_part_type expected(0, negative_one);
        EXPECT_EQ(result, expected);
    }
    {
        dual_part_type original(negative_one, negative_one);
        dual_part_type result = original << (part_type_width * 2 - 1);
        dual_part_type expected(0, most_negative_value);
        EXPECT_EQ(result, expected);
    }
    {
        dual_part_type original(negative_two, negative_one);
        dual_part_type result = original << (part_type_width * 2 - 1);
        dual_part_type expected(0, 0);
        EXPECT_EQ(result, expected);
    }
    {
        dual_part_type original(1, 0);
        dual_part_type result = original << (part_type_width * 2 - 1);
        dual_part_type expected(0, most_negative_value);
        EXPECT_EQ(result, expected);
    }
}

TEST(IntegerImplPartType, MultiplyAndExpandUnsigned) {
    {
        std::array<part_type, 2> result = multiply_and_expand_unsigned(15, 2);
        std::array<part_type, 2> expected = {30, 0};
        EXPECT_EQ(result, expected);
    }
    {
        std::array<part_type, 2> result = multiply_and_expand_unsigned(negative_one, negative_one);
        std::array<part_type, 2> expected = {1, negative_two};
        EXPECT_EQ(result, expected);
    }
    {
        std::array<part_type, 2> result = multiply_and_expand_unsigned(40, 1);
        std::array<part_type, 2> expected = {40, 0};
        EXPECT_EQ(result, expected);
    }
    {
        std::array<part_type, 2> result = multiply_and_expand_unsigned(negative_one, negative_two);
        std::array<part_type, 2> expected = {2, negative_two - 1u};
        EXPECT_EQ(result, expected);
    }
    {
        std::array<part_type, 2> result = multiply_and_expand_unsigned(negative_one, 0);
        std::array<part_type, 2> expected = {0, 0};
        EXPECT_EQ(result, expected);
    }
}