#include "big/detail/macros.h"
#include "big/detail/part_type.h"
#include "big/detail/view.h"

#include <gtest/gtest.h>

#include <random>
#include <set>
#include <unordered_set>

using namespace big::detail;

struct VectorHash {
    std::size_t operator()(const std::vector<part_type>& v) const {
        std::hash<part_type> hasher;
        std::size_t seed = 0;
        for (part_type i : v) {
            seed ^= hasher(i) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        }
        return seed;
    }
};

class IntegerImplViewFixture : public ::testing::Test {
  public:
    IntegerImplViewFixture() {
        std::set<part_type> assorted_parts = {part_type(0), most_negative_value, most_positive_value, negative_one, negative_two, part_type(1)};
        std::mt19937 generator;
        std::uniform_int_distribution<part_type> distribution(0, negative_one);
        auto random_part = [&] { return distribution(generator); };
        std::vector<part_type> empty_integer;
        // unordered set needed so order is random
        assorted_integers = {empty_integer};
        std::vector<part_type> large_integer;
        for (int i = 0; i < 10; i++) {
            large_integer.push_back(random_part());
        }
        assorted_integers.insert(large_integer);
        for (int i = 0; i < 4; i++) {
            std::vector<std::vector<part_type>> new_integers;
            auto new_assorted_parts = assorted_parts;
            new_assorted_parts.insert(random_part());
            for (part_type part : new_assorted_parts) {
                for (std::vector<part_type> existing_integer : assorted_integers) {
                    existing_integer.push_back(part);
                    new_integers.push_back(existing_integer);
                }
            }
            assorted_integers.insert(new_integers.begin(), new_integers.end());
        }
        assorted_integers.erase(empty_integer);

        for (const auto& v : assorted_integers) {
            assorted_views.push_back(consolidate(make_view(v)));
        }

        for (auto part : assorted_parts) {
            std::vector<part_type> v;
            v.push_back(part);
            less_assorted_integers.insert(v);
        }
        auto it = assorted_integers.begin();
        while (less_assorted_integers.size() < 200) {
            less_assorted_integers.insert(*it);
            ++it;
        }
        for (const auto& v : less_assorted_integers) {
            less_assorted_views.push_back(consolidate(make_view(v)));
        }
    }

  protected:
    std::unordered_set<std::vector<part_type>, VectorHash> assorted_integers, less_assorted_integers;
    std::vector<integer_view> assorted_views, less_assorted_views;
    int visible_by_all_tests = 5;
};

TEST(IntegerImplView, MakeView) {
    std::vector<part_type> a = {1, 2, 3, 4};
    integer_view b = make_view(a);
    std::vector<part_type> c = make_vector(b);
    ASSERT_EQ(a, c);
}

TEST(IntegerImplView, Front) {
    std::vector<part_type> a = {1, 2, 3, 4};
    ASSERT_EQ(a.front(), 1);
    ASSERT_EQ(a.back(), 4);
}

TEST(IntegerImplView, SignOfPart) {
    part_type a = 1;
    part_type b = 0;
    part_type c = negative_one;
    ASSERT_EQ(sign_of(a), 0);
    ASSERT_EQ(sign_of(b), 0);
    ASSERT_EQ(sign_of(c), 1);
}

TEST(IntegerImplView, SignOfView) {
    std::vector<part_type> a = {1, 2, 3, 1};
    std::vector<part_type> b = {1, 2, 3, 0};
    std::vector<part_type> c = {1, 2, 3, 0};
    c.back()--;
    ASSERT_EQ(sign_of(make_view(a)), 0);
    ASSERT_EQ(sign_of(make_view(b)), 0);
    ASSERT_EQ(sign_of(make_view(c)), 1);
}

TEST(IntegerImplView, SpreadBool) {
    part_type a = spread_bool(0);
    part_type b = spread_bool(1);
    ASSERT_EQ(a, 0);
    ASSERT_EQ(b, negative_one);
}

TEST(IntegerImplView, Consolidate) {
    {
        std::vector<part_type> v = {5, 0, negative_one};
        integer_view subject = make_view(v);
        integer_view expected = {subject.data, 3};
        ASSERT_TRUE(same(consolidate(subject), expected));
        ASSERT_TRUE(same(consolidate(consolidate(subject)), expected));
    }
    {
        std::vector<part_type> v = {5, 0, 0};
        integer_view subject = make_view(v);
        integer_view expected = {subject.data, 1};
        ASSERT_TRUE(same(consolidate(subject), expected));
        ASSERT_TRUE(same(consolidate(consolidate(subject)), expected));
    }
    {
        std::vector<part_type> v = {0};
        integer_view subject = make_view(v);
        integer_view expected = {subject.data, 1};
        ASSERT_TRUE(same(consolidate(subject), expected));
        ASSERT_TRUE(same(consolidate(consolidate(subject)), expected));
    }
    {
        std::vector<part_type> v = {negative_two, negative_one, negative_one};
        integer_view subject = make_view(v);
        integer_view expected = {subject.data, 1};
        ASSERT_TRUE(same(consolidate(subject), expected));
        ASSERT_TRUE(same(consolidate(consolidate(subject)), expected));
    }
    {
        std::vector<part_type> v = {negative_two, 0, 0};
        integer_view subject = make_view(v);
        integer_view expected = {subject.data, 2};
        ASSERT_TRUE(same(consolidate(subject), expected));
        ASSERT_TRUE(same(consolidate(consolidate(subject)), expected));
    }
    {
        std::vector<part_type> v = {negative_two, 2, 3};
        integer_view subject = make_view(v);
        integer_view expected = {subject.data, 3};
        ASSERT_TRUE(same(consolidate(subject), expected));
        ASSERT_TRUE(same(consolidate(consolidate(subject)), expected));
    }
    // TODO(ben.hansell): fix clang-format here
    if
        BNI_CONSTEXPR_CXX17(sizeof(part_type) == 8) {
            {
                std::vector<part_type> subject{(part_type)9999999999999999999ull, 0};
                ASSERT_TRUE(same(consolidate(make_view(subject)), make_view(subject)));
            }
        }
}

TEST(IntegerImplView, MakeWriter) {
    std::vector<part_type> original = {1, 2, 3, 1};
    integer_view intermediate = make_view(original);
    std::vector<part_type> copy;
    std::function<void(integer_view)> writer = make_writer(&copy);
    writer(intermediate);
    ASSERT_EQ(original, copy);
}

TEST(IntegerImplView, ExtendSign) {
    { // original value is positive
        std::vector<part_type> positive_original = {1, 2, 3, 1};
        std::vector<part_type> positive_extended;
        std::function<void(integer_view)> writer = make_writer(&positive_extended);
        extend_sign(make_view(positive_original), 7, writer);
        std::vector<part_type> expected = {1, 2, 3, 1, 0, 0, 0};

        ASSERT_EQ(positive_extended, expected);
        extend_sign(make_view(positive_original), 5, writer);
        expected = {1, 2, 3, 1, 0};
        ASSERT_EQ(positive_extended, expected);
        extend_sign(make_view(positive_original), 4, writer);
        ASSERT_EQ(positive_extended, positive_original);
    }
    { // original value is negative
        std::vector<part_type> negative_original = {1, 2, 3, negative_two};
        std::vector<part_type> negative_extended;
        std::function<void(integer_view)> writer = make_writer(&negative_extended);
        extend_sign(make_view(negative_original), 7, writer);
        std::vector<part_type> expected = {1, 2, 3, negative_two, negative_one, negative_one, negative_one};
        ASSERT_EQ(negative_extended, expected);
        extend_sign(make_view(negative_original), 5, writer);
        expected = {1, 2, 3, negative_two, negative_one};
        ASSERT_EQ(negative_extended, expected);
        extend_sign(make_view(negative_original), 4, writer);
        ASSERT_EQ(negative_extended, negative_original);
    }
}
TEST(IntegerImplView, ExtendSign2) {
    {
        std::vector<part_type> original = {negative_one};
        std::vector<part_type> negative_extended;
        extend_sign(make_view(original), 7, make_writer(&negative_extended));
        std::vector<part_type> expected(7, negative_one);
        ASSERT_EQ(negative_extended, expected);
    }
    // shrinking a value using extend_sign is undefined behaviour
    // extending the sign of a integer_view whose size is 0 is undefined behaviour, for now
}

TEST(IntegerImplView, Complement) {
    {
        std::vector<part_type> source = {0, 0};
        std::vector<part_type> expected = {negative_one, negative_one};
        std::vector<part_type> result;
        complement(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_one, 0};
        std::vector<part_type> expected = {0, negative_one};
        std::vector<part_type> result;
        complement(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_two, 0};
        std::vector<part_type> expected = {1, negative_one};
        std::vector<part_type> result;
        complement(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
}

TEST(IntegerImplView, IncrementUnsigned) {
    {
        std::vector<part_type> source = {0};
        std::vector<part_type> expected = {1};
        std::vector<part_type> result;
        increment_unsigned(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_one};
        std::vector<part_type> expected = {0, 1};
        std::vector<part_type> result;
        increment_unsigned(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {0, 0};
        std::vector<part_type> expected = {1, 0};
        std::vector<part_type> result;
        increment_unsigned(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_one, negative_one};
        std::vector<part_type> expected = {0, 0, 1};
        std::vector<part_type> result;
        increment_unsigned(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_two, negative_two};
        std::vector<part_type> expected = {negative_one, negative_two};
        std::vector<part_type> result;
        increment_unsigned(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_one, negative_two};
        std::vector<part_type> expected = {0, negative_one};
        std::vector<part_type> result;
        increment_unsigned(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
}

TEST(IntegerImplView, ShiftLeftOneBit) {
    {
        std::vector<part_type> source = {0};
        std::vector<part_type> expected = {0};
        std::vector<part_type> result;
        shift_left_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_one};
        std::vector<part_type> expected = {negative_one << 1, 1};
        std::vector<part_type> result;
        shift_left_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {1};
        std::vector<part_type> expected = {2};
        std::vector<part_type> result;
        shift_left_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {0, 1};
        std::vector<part_type> expected = {0, 2};
        std::vector<part_type> result;
        shift_left_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {1, 1};
        std::vector<part_type> expected = {2, 2};
        std::vector<part_type> result;
        shift_left_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {1, 0, negative_one, 3};
        std::vector<part_type> expected = {2, 0, negative_one << 1, 7};
        std::vector<part_type> result;
        shift_left_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {1, 0, negative_one, negative_one};
        std::vector<part_type> expected = {2, 0, negative_one << 1, negative_one, 1};
        std::vector<part_type> result;
        shift_left_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> value = {1, 1};
        std::vector<part_type> expected = {1, 1};
        for (std::size_t i = 0; i < part_type_width - 1; i++) {
            shift_left_one_bit(make_view(value), make_writer(&value));
            expected[0] <<= 1;
            expected[1] <<= 1;
            ASSERT_EQ(expected, value);
        }
        shift_left_one_bit(make_view(value), make_writer(&value));
        expected = {0, 1, 1};
        ASSERT_EQ(expected, value);
    }
}

TEST(IntegerImplView, ShiftRightOneBit) {
    {
        std::vector<part_type> source = {0};
        std::vector<part_type> expected = {0};
        std::vector<part_type> result;
        shift_right_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }

    {
        std::vector<part_type> source = {1};
        std::vector<part_type> expected = {0};
        std::vector<part_type> result;
        shift_right_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_one};
        std::vector<part_type> expected = {negative_one >> 1};
        std::vector<part_type> result;
        shift_right_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_one, negative_one};
        std::vector<part_type> expected = {negative_one, negative_one >> 1};
        std::vector<part_type> result;
        shift_right_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {negative_two, negative_one};
        std::vector<part_type> expected = {negative_one, negative_one >> 1};
        std::vector<part_type> result;
        shift_right_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> source = {most_negative_value, most_negative_value};
        std::vector<part_type> expected = {most_negative_value >> 1, most_negative_value >> 1};
        std::vector<part_type> result;
        shift_right_one_bit(make_view(source), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> value = {most_negative_value, most_negative_value};
        std::vector<part_type> expected = {most_negative_value, most_negative_value};
        for (std::size_t i = 0; i < part_type_width - 1; i++) {
            shift_right_one_bit(make_view(value), make_writer(&value));
            expected[0] >>= 1;
            expected[1] >>= 1;
            ASSERT_EQ(expected, value);
        }
        shift_right_one_bit(make_view(value), make_writer(&value));
        expected = {most_negative_value, 0};
        ASSERT_EQ(expected, value);
    }
}

TEST(IntegerImplView, LessThanUnsigned) {
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0};
        bool result = less_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {1};
        std::vector<part_type> b = {0};
        bool result = less_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {1};
        bool result = less_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0, 1};
        bool result = less_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {negative_one};
        std::vector<part_type> b = {0, 1};
        bool result = less_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {1, 0, 1};
        std::vector<part_type> b = {negative_one};
        bool result = less_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
}

TEST(IntegerImplView, GreaterThanUnsigned) {
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0};
        bool result = greater_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {1};
        std::vector<part_type> b = {1};
        bool result = greater_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {1};
        std::vector<part_type> b = {0};
        bool result = greater_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {1};
        bool result = greater_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0, 1};
        bool result = greater_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {negative_one};
        std::vector<part_type> b = {0, 1};
        bool result = greater_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {1, 0, 1};
        std::vector<part_type> b = {negative_one};
        bool result = greater_than_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
}

TEST(IntegerImplView, LessThanOrEqualToUnsigned) {
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0};
        bool result = less_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {1};
        std::vector<part_type> b = {1};
        bool result = less_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {1};
        std::vector<part_type> b = {0};
        bool result = less_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {1};
        bool result = less_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0, 1};
        bool result = less_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {negative_one};
        std::vector<part_type> b = {0, 1};
        bool result = less_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {1, 0, 1};
        std::vector<part_type> b = {negative_one};
        bool result = less_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
}

TEST(IntegerImplView, GreaterThanOrEqualToUnsigned) {
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0};
        bool result = greater_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {1};
        std::vector<part_type> b = {1};
        bool result = greater_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {1};
        std::vector<part_type> b = {0};
        bool result = greater_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {1};
        bool result = greater_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0, 1};
        bool result = greater_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {negative_one};
        std::vector<part_type> b = {0, 1};
        bool result = greater_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {1, 0, 1};
        std::vector<part_type> b = {negative_one};
        bool result = greater_than_or_equal_to_unsigned(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
}

TEST(IntegerImplView, EqualTo) {
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0};
        bool result = equal_to(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {1};
        std::vector<part_type> b = {1};
        bool result = equal_to(make_view(a), make_view(b));
        ASSERT_EQ(result, true);
    }
    {
        std::vector<part_type> a = {1};
        std::vector<part_type> b = {0};
        bool result = equal_to(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {1};
        bool result = equal_to(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {0, 1};
        bool result = equal_to(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {negative_one};
        std::vector<part_type> b = {0, 1};
        bool result = equal_to(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
    {
        std::vector<part_type> a = {1, 0, 1};
        std::vector<part_type> b = {negative_one};
        bool result = equal_to(make_view(a), make_view(b));
        ASSERT_EQ(result, false);
    }
}

TEST(IntegerImplView, UnaryMinus) {
    {
        std::vector<part_type> original = {0};
        std::vector<part_type> expected = {0};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> original = {1};
        std::vector<part_type> expected = {negative_one};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> original = {negative_one};
        std::vector<part_type> expected = {1};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> original = {negative_two};
        std::vector<part_type> expected = {2};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> original = {2};
        std::vector<part_type> expected = {negative_two};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> original = {most_positive_value};
        std::vector<part_type> expected = {most_negative_value + 1};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> original = {most_negative_value};
        std::vector<part_type> expected = {most_negative_value, 0};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> original = {0, most_negative_value};
        std::vector<part_type> expected = {0, most_negative_value, 0};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> original = {negative_one, most_positive_value};
        std::vector<part_type> expected = {1, most_negative_value};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
    {
        std::vector<part_type> original = {negative_one, negative_two};
        std::vector<part_type> expected = {1, 1};
        std::vector<part_type> result;
        unary_minus(make_view(original), make_writer(&result));
        ASSERT_EQ(expected, result);
    }
}

TEST(IntegerImplView, Add) {
    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {15};
        std::vector<part_type> expected_sum = {15};
        std::vector<part_type> actual_sum;
        add(make_view(a), make_view(b), make_writer(&actual_sum));
        ASSERT_EQ(expected_sum, actual_sum);
    }
    {
        std::vector<part_type> a = {negative_two};
        std::vector<part_type> b = {1};
        std::vector<part_type> expected_sum = {negative_one};
        std::vector<part_type> actual_sum;
        add(make_view(a), make_view(b), make_writer(&actual_sum));
        ASSERT_EQ(expected_sum, actual_sum);
    }
    {
        std::vector<part_type> a = {negative_one};
        std::vector<part_type> b = {negative_one};
        std::vector<part_type> expected_sum = {negative_two};
        std::vector<part_type> actual_sum;
        add(make_view(a), make_view(b), make_writer(&actual_sum));
        ASSERT_EQ(expected_sum, actual_sum);
    }
    {
        std::vector<part_type> a = {negative_two};
        std::vector<part_type> b = {1};
        std::vector<part_type> expected_sum = {negative_one};
        std::vector<part_type> actual_sum;
        add(make_view(a), make_view(b), make_writer(&actual_sum));
        ASSERT_EQ(expected_sum, actual_sum);
    }
    {
        std::vector<part_type> a = {0, 1};
        std::vector<part_type> b = {1};
        std::vector<part_type> expected_sum = {1, 1};
        std::vector<part_type> actual_sum;
        add(make_view(a), make_view(b), make_writer(&actual_sum));
        ASSERT_EQ(expected_sum, actual_sum);
    }
    {
        std::vector<part_type> a = {most_positive_value};
        std::vector<part_type> b = {1};
        std::vector<part_type> expected_sum = {most_negative_value, 0};
        std::vector<part_type> actual_sum;
        add(make_view(a), make_view(b), make_writer(&actual_sum));
        ASSERT_EQ(expected_sum, actual_sum);
    }
    {
        std::vector<part_type> a = {most_negative_value};
        std::vector<part_type> b = {negative_one};
        std::vector<part_type> expected_sum = {most_positive_value, negative_one};
        std::vector<part_type> actual_sum;
        add(make_view(a), make_view(b), make_writer(&actual_sum));
        ASSERT_EQ(expected_sum, actual_sum);
    }
    {
        std::vector<part_type> a = {negative_one, most_positive_value};
        std::vector<part_type> b = {1};
        std::vector<part_type> expected_sum = {0, most_negative_value, 0};
        std::vector<part_type> actual_sum;
        add(make_view(a), make_view(b), make_writer(&actual_sum));
        ASSERT_EQ(expected_sum, actual_sum);
    }
}

TEST(IntegerImplView, Subtract) {

    {
        std::vector<part_type> a = {0};
        std::vector<part_type> b = {15};
        std::vector<part_type> expected_difference = {part_type(part_type(0) - part_type(15))};
        std::vector<part_type> actual_difference;
        subtract(make_view(a), make_view(b), make_writer(&actual_difference));
        ASSERT_EQ(expected_difference, actual_difference);
    }

    // more test cases needed
}

TEST_F(IntegerImplViewFixture, AddSubtractMinus) {
    integer_view previous = assorted_views.back();
    for (integer_view subject : assorted_views) {

        // test a - a == 0
        std::vector<part_type> expected_difference = {0};
        std::vector<part_type> actual_difference;
        subtract(subject, subject, make_writer(&actual_difference));
        ASSERT_EQ(expected_difference, actual_difference);

        // test --a == a
        std::vector<part_type> minus_of_subject;
        unary_minus(subject, make_writer(&minus_of_subject));
        std::vector<part_type> minus_of_minus_of_subject;
        unary_minus(make_view(minus_of_subject), make_writer(&minus_of_minus_of_subject));
        ASSERT_EQ(minus_of_minus_of_subject, make_vector(subject));

        // test a + -b == a - b == -b + a == - (-a + b)
        integer_view a = subject, b = previous;
        std::vector<part_type> minus_b; // -b
        unary_minus(b, make_writer(&minus_b));
        std::vector<part_type> minus_a; // -a
        unary_minus(a, make_writer(&minus_a));
        std::vector<part_type> a_plus_minus_b; // a + -b
        add(a, make_view(minus_b), make_writer(&a_plus_minus_b));
        std::vector<part_type> a_minus_b; // a - b
        subtract(a, b, make_writer(&a_minus_b));
        std::vector<part_type> minus_b_plus_a; // -b + a
        add(make_view(minus_b), a, make_writer(&minus_b_plus_a));
        std::vector<part_type> minus_a_plus_b; // -a + b
        add(make_view(minus_a), b, make_writer(&minus_a_plus_b));
        std::vector<part_type> minus__minus_a_plus_b; // -(-a + b)
        unary_minus(make_view(minus_a_plus_b), make_writer(&minus__minus_a_plus_b));
        ASSERT_EQ(a_plus_minus_b, a_minus_b);             // a + -b == a - b
        ASSERT_EQ(a_minus_b, minus_b_plus_a);             // a - b == -b + a
        ASSERT_EQ(minus_b_plus_a, minus__minus_a_plus_b); // -b + a == - (-a + b)

        previous = subject;
    }
}

TEST(IntegerImplView, Multiply) {
    {
        std::vector<part_type> a{7};
        std::vector<part_type> square;
        std::vector<part_type> expected{49};
        multiply(make_view(a), make_view(a), make_writer(&square));
        EXPECT_EQ(expected, square);
    }
    {
        std::vector<part_type> a{7};
        std::vector<part_type> b{0};
        std::vector<part_type> product;
        std::vector<part_type> expected{0};
        multiply(make_view(a), make_view(b), make_writer(&product));
        EXPECT_EQ(expected, product);
    }
    {
        std::vector<part_type> a{0, 1};
        std::vector<part_type> b{1};
        std::vector<part_type> product;
        std::vector<part_type> expected{0, 1};
        multiply(make_view(a), make_view(b), make_writer(&product));
        EXPECT_EQ(expected, product);
    }
    {
        std::vector<part_type> a{negative_one};
        std::vector<part_type> b{negative_one};
        std::vector<part_type> product;
        std::vector<part_type> expected{1};
        multiply(make_view(a), make_view(b), make_writer(&product));
        EXPECT_EQ(expected, product);
    }
    {
        std::vector<part_type> a{negative_two};
        std::vector<part_type> square;
        std::vector<part_type> expected{4};
        multiply(make_view(a), make_view(a), make_writer(&square));
        EXPECT_EQ(expected, square);
    }
    {
        std::vector<part_type> a{1};
        std::vector<part_type> b{2};
        std::vector<part_type> product;
        std::vector<part_type> expected{2};
        multiply(make_view(a), make_view(b), make_writer(&product));
        EXPECT_EQ(expected, product);
    }
    {
        std::vector<part_type> a{negative_one};
        std::vector<part_type> b{2};
        std::vector<part_type> product;
        std::vector<part_type> expected{negative_two};
        multiply(make_view(a), make_view(b), make_writer(&product));
        EXPECT_EQ(expected, product);
    }

    std::vector<std::vector<part_type>> candidates = {{5}, {5, 6, 7}, {negative_one}, {negative_one, 16}, {0}, {negative_two}};
    for (const std::vector<part_type>& x : candidates) {
        std::vector<part_type> five{5};
        std::vector<part_type> two{2};
        std::vector<part_type> ten{10};

        std::vector<part_type> five_times_x_times_two_times_x;
        multiply(make_view(five), make_view(x), make_writer(&five_times_x_times_two_times_x));
        multiply(make_view(five_times_x_times_two_times_x), make_view(two), make_writer(&five_times_x_times_two_times_x));
        multiply(make_view(five_times_x_times_two_times_x), make_view(x), make_writer(&five_times_x_times_two_times_x));

        std::vector<part_type> ten_times_x_times_x;
        multiply(make_view(ten), make_view(x), make_writer(&ten_times_x_times_x));
        multiply(make_view(ten_times_x_times_x), make_view(x), make_writer(&ten_times_x_times_x));

        EXPECT_EQ(ten_times_x_times_x, five_times_x_times_two_times_x);
    }
}

TEST_F(IntegerImplViewFixture, Multiply) {
    for (integer_view x : less_assorted_views) {
        std::vector<part_type> five{5};
        std::vector<part_type> two{2};
        std::vector<part_type> ten{10};

        std::vector<part_type> five_times_x_times_two_times_x;
        multiply(make_view(five), x, make_writer(&five_times_x_times_two_times_x));
        multiply(make_view(five_times_x_times_two_times_x), make_view(two), make_writer(&five_times_x_times_two_times_x));
        multiply(make_view(five_times_x_times_two_times_x), x, make_writer(&five_times_x_times_two_times_x));

        std::vector<part_type> ten_times_x_times_x;
        multiply(make_view(ten), x, make_writer(&ten_times_x_times_x));
        multiply(make_view(ten_times_x_times_x), x, make_writer(&ten_times_x_times_x));

        EXPECT_EQ(ten_times_x_times_x, five_times_x_times_two_times_x);
    }
}

TEST(IntegerImplView, Divide) {
    {
        std::vector<part_type> a{7};
        std::vector<part_type> b{4};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{1};
        std::vector<part_type> expected_remainder{3};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{100};
        std::vector<part_type> b{10};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{10};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{0};
        std::vector<part_type> b{1};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{0};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{0};
        std::vector<part_type> b{negative_one};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{0};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{0};
        std::vector<part_type> b{negative_one, negative_two};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{0};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{1, 1};
        std::vector<part_type> b{1};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result = {1, 1};
        std::vector<part_type> expected_remainder = {0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{negative_one, negative_two};
        std::vector<part_type> b{1};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result = {negative_one, negative_two};
        std::vector<part_type> expected_remainder = {0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{negative_one, negative_two};
        std::vector<part_type> b{negative_one, negative_two};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{1};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{5};
        std::vector<part_type> b{negative_one};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{part_type(0) - part_type(5)};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{negative_one, 1};
        std::vector<part_type> b{negative_one, 1};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{1};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{negative_one, negative_two, 0};
        std::vector<part_type> b{negative_one, negative_two, 0};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{1};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{negative_one, negative_one, 0};
        std::vector<part_type> b{negative_one, negative_one, 0};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{1};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{negative_one};
        std::vector<part_type> b{negative_one};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{1};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{5, negative_two};
        std::vector<part_type> b{6, negative_two};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{1};
        std::vector<part_type> expected_remainder{negative_one};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{negative_one};
        std::vector<part_type> b{2};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{0};
        std::vector<part_type> expected_remainder{negative_one};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
    {
        std::vector<part_type> a{negative_one};
        std::vector<part_type> b{1};
        std::vector<part_type> result;
        std::vector<part_type> remainder;
        std::vector<part_type> expected_result{negative_one};
        std::vector<part_type> expected_remainder{0};
        divide(make_view(a), make_view(b), make_writer(&result), make_writer(&remainder));
        EXPECT_EQ(result, expected_result);
        EXPECT_EQ(remainder, expected_remainder);
    }
}

TEST_F(IntegerImplViewFixture, Divide) {
    for (integer_view x : less_assorted_views) {
        {
            std::vector<part_type> result;
            std::vector<part_type> remainder;
            std::vector<part_type> denominator = {1};
            std::vector<part_type> expected_remainder = {0};
            divide(x, make_view(denominator), make_writer(&result), make_writer(&remainder));
            EXPECT_EQ(result, make_vector(x));
            EXPECT_EQ(remainder, expected_remainder);
        }
        if (x.data[0] == 0 && x.size == 1) {
            continue;
        }
        {
            std::vector<part_type> result;
            std::vector<part_type> remainder;
            std::vector<part_type> expected_result = {1};
            std::vector<part_type> expected_remainder = {0};
            divide(x, x, make_writer(&result), make_writer(&remainder));
            EXPECT_EQ(result, expected_result);
            EXPECT_EQ(remainder, expected_remainder);
        }
        // TODO(ben.hansell): consider more complicated test cases such as (x / 2 / 5) ?=== (x / 10)
    }
}

TEST(IntegerImplView, Power) {
    {
        std::vector<part_type> significand{7};
        unsigned exponent = 2;
        std::vector<part_type> result;
        std::vector<part_type> expected{49};
        power(make_view(significand), exponent, make_writer(&result));
        EXPECT_EQ(expected, result);
    }
    {
        std::vector<part_type> significand{part_type(0u) - part_type(7u)};
        unsigned exponent = 2;
        std::vector<part_type> result;
        std::vector<part_type> expected{49};
        power(make_view(significand), exponent, make_writer(&result));
        EXPECT_EQ(expected, result);
    }
    {
        std::vector<part_type> significand{part_type(0u) - part_type(5u)};
        unsigned exponent = 3;
        std::vector<part_type> result;
        std::vector<part_type> minus_expected = {125};
        std::vector<part_type> expected;
        unary_minus(make_view(minus_expected), make_writer(&expected));
        power(make_view(significand), exponent, make_writer(&result));
        EXPECT_EQ(expected, result);
    }
}

TEST(IntegerImplView, FromString) {
    ASSERT_THROW(from_string("not a number", [](integer_view) {}), std::invalid_argument);
    ASSERT_THROW(from_string("", [](integer_view) {}), std::invalid_argument); // not sure about this one
    ASSERT_THROW(from_string("-", [](integer_view) {}), std::invalid_argument);
    ASSERT_THROW(from_string("4-", [](integer_view) {}), std::invalid_argument);
    ASSERT_THROW(from_string("4 ", [](integer_view) {}), std::invalid_argument);
    ASSERT_THROW(from_string("--4", [](integer_view) {}), std::invalid_argument);
    ASSERT_THROW(from_string("5.", [](integer_view) {}), std::invalid_argument);
    ASSERT_THROW(from_string("5.0", [](integer_view) {}), std::invalid_argument);
    ASSERT_NO_THROW(from_string("125", [](integer_view) {}));
    ASSERT_NO_THROW(from_string("0", [](integer_view) {}));
    ASSERT_NO_THROW(from_string("-0", [](integer_view) {}));
    {
        std::vector<part_type> converted;
        from_string("102", make_writer(&converted));
        std::vector<part_type> expected = {102u};
        ASSERT_EQ(converted, expected);
    }
    {
        std::vector<part_type> converted;
        from_string("0102", make_writer(&converted));
        std::vector<part_type> expected = {102u};
        ASSERT_EQ(converted, expected);
    }
    {
        std::vector<part_type> converted;
        from_string("-75", make_writer(&converted));
        std::vector<part_type> expected = {part_type(0u) - part_type(75u)};
        ASSERT_EQ(converted, expected);
    }
    {
        std::vector<part_type> converted;
        from_string("-1", make_writer(&converted));
        std::vector<part_type> expected = {negative_one};
        ASSERT_EQ(converted, expected);
    }
    {
        std::vector<part_type> a;
        std::vector<part_type> a_plus_one;
        from_string("016757657638765387653876538763534202003846845468468468", make_writer(&a));
        from_string("16757657638765387653876538763534202003846845468468469", make_writer(&a_plus_one));
        std::vector<part_type> a_plus_one_again;
        increment_unsigned(make_view(a), make_writer(&a_plus_one_again));
        ASSERT_EQ(a_plus_one, a_plus_one_again);
    }
    {
        std::vector<part_type> a;
        std::vector<part_type> a_plus_one;
        from_string("9999999999999999999", make_writer(&a));
        from_string("10000000000000000000", make_writer(&a_plus_one));
        std::vector<part_type> a_plus_one_again;
        increment_unsigned(make_view(a), make_writer(&a_plus_one_again));
        ASSERT_EQ(a_plus_one, a_plus_one_again);
    }
}

TEST(IntegerImplView, ToString) {
    {
        std::vector<part_type> value{0};
        std::string value_as_string = to_string(make_view(value));
        ASSERT_EQ(value_as_string, "0");
    }
    {
        std::vector<part_type> value{negative_one};
        std::string value_as_string = to_string(make_view(value));
        ASSERT_EQ(value_as_string, "-1");
    }
    {
        std::vector<part_type> value{negative_two};
        std::string value_as_string = to_string(make_view(value));
        ASSERT_EQ(value_as_string, "-2");
    }
    {
        std::vector<part_type> value{100};
        std::string value_as_string = to_string(make_view(value));
        ASSERT_EQ(value_as_string, "100");
    }
    {
        std::vector<part_type> value{101};
        std::string value_as_string = to_string(make_view(value));
        ASSERT_EQ(value_as_string, "101");
    }
    if (sizeof(part_type) == 8) {
        {
            std::vector<part_type> value{1, 1};
            std::string value_as_string = to_string(make_view(value));
            ASSERT_EQ(value_as_string, "18446744073709551617");
        }
        {
            std::vector<part_type> value{most_positive_value};
            std::string value_as_string = to_string(make_view(value));
            ASSERT_EQ(value_as_string, "9223372036854775807");
        }
        {
            std::vector<part_type> value{0, 1};
            std::string value_as_string = to_string(make_view(value));
            ASSERT_EQ(value_as_string, "18446744073709551616");
        }
    }
}

TEST(IntegerImplView, ToAndFromString) {
    std::vector<std::string> candidates = {"0", "1", "-1"};
    std::mt19937 generator;
    std::uniform_int_distribution<int> distribution(0, 9);
    auto random_digit = [&] { return std::to_string(distribution(generator)); };
    for (unsigned number_of_digits = 1; number_of_digits < 150; number_of_digits += 17) {
        std::string candidate;

        for (unsigned i = 0; i < number_of_digits; i++) {
            candidate += random_digit();
        }
        if (candidate[0] == '0') {
            candidate = "9" + candidate;
        }
        candidates.push_back(candidate);
        candidates.push_back("-" + candidate);
    }

    for (const std::string& candidate : candidates) {
        std::vector<part_type> value;
        from_string(candidate, make_writer(&value));
        std::string candidate_copy = to_string(make_view(value));
        ASSERT_EQ(candidate, candidate_copy);
    }
}

TEST(IntegerImplView, Mix) {
    std::vector<part_type> a;
    std::vector<part_type> b;
    std::vector<part_type> c;
    std::vector<part_type> d;
    from_string("10000000000000000000000000", make_writer(&a));
    from_string("6", make_writer(&b));
    from_string("-2", make_writer(&c));
    add(make_view(a), make_view(b), make_writer(&d));
    multiply(make_view(d), make_view(c), make_writer(&d));
    std::string result = to_string(make_view(d));
    std::string expected = "-20000000000000000000000012";
}