#include <gtest/gtest.h>

#include <iostream>

#include "big/detail/part_type.h"
#include "big/detail/macros.h"

int main(int argc, char** argv) {
    std::cout << "sizeof(part_type) is " << sizeof(big::detail::part_type) << std::endl;
    std::cout << "__cplusplus is " << __cplusplus << std::endl;
    #ifdef _MSC_VER
    std::cout << "_MSC_VER is " << _MSC_VER << std::endl;
    #endif
    #ifdef _MSVC_LANG
    std::cout << "_MSVC_LANG is " << _MSVC_LANG  << std::endl;
    #endif
    std::cout << "BNI_CXX_VERSION is " << BNI_CXX_VERSION << std::endl;
    std::cout << std::endl;
    
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}