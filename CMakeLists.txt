cmake_minimum_required(VERSION 3.5.1)

project(big-numbers)

if(NOT DEFINED CMAKE_CXX_STANDARD)
    set(CMAKE_CXX_STANDARD 11)
endif()

if (MSVC)
    add_compile_options(/W4)
else()
    # add_compile_options(-Wall -Wextra -Wpedantic)
    # extra compiler warnings disabled in favour of clang-tidy
endif()

FILE(GLOB big_integer_sources src/*.cpp)
add_library(big-integer ${big_integer_sources})
target_include_directories(big-integer PUBLIC include PRIVATE src)

# TODO(ben.hansel): make an empty library called big-numbers which depends on big-integer (and in future big-quotient)

add_subdirectory(test)
add_subdirectory(benchmark)