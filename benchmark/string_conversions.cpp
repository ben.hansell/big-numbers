#include <benchmark/benchmark.h>

#include <sstream>

#include "big/integer.h"

using namespace big::literals;

static void Big_ConstructFromStringLiteral(benchmark::State& state) {
    const char* source_string = "-010000000000000000000000000";
    for (auto _ : state)
        big::integer number = source_string;
}
BENCHMARK(Big_ConstructFromStringLiteral);

static void Small_ConstructFromStringLiteral(benchmark::State& state) {
    const char* source_string = "100";
    for (auto _ : state)
        big::integer number = source_string;
}
BENCHMARK(Small_ConstructFromStringLiteral);

static void Big_ConvertToString(benchmark::State& state) {
    big::integer source_integer = -010000000000000000000000000_bi;
    std::stringstream ss;
    for (auto _ : state)
        ss << source_integer;
}
BENCHMARK(Big_ConvertToString);

static void Small_ConvertToString(benchmark::State& state) {
    big::integer source_integer = 40000000_bi;
    std::stringstream ss;
    for (auto _ : state)
        ss << source_integer;
}
BENCHMARK(Small_ConvertToString);