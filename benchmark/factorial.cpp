#include <benchmark/benchmark.h>

#include "big/integer.h"

using namespace big::literals;

// TODO(ben.hansell): this should take unsigned as input
big::integer
factorial(const big::integer& N) {
    if (N == 0_bi) {
        return 1_bi;
    }
    return N * factorial(N - 1_bi);
}

static void Big_Factorial(benchmark::State& state) {
    big::integer N = 200_bi;
    for (auto _ : state)
        auto result = factorial(N);
}

BENCHMARK(Big_Factorial);
static void Small_Factorial(benchmark::State& state) {
    big::integer N = 20_bi; // smallest factorial less than 2^62
    for (auto _ : state)
        auto result = factorial(N);
}
BENCHMARK(Small_Factorial);