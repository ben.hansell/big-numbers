#pragma once

#include <cstddef>

namespace big {
namespace detail {

// same as alloc and free, but the pointer is guaranteed to be aligned such that the least significant bit is zero
// then the pointer can be tagged: https://en.wikipedia.org/wiki/Tagged_pointer
void* aligned_alloc_2(std::size_t size);
void aligned_free_2(void* ptr);

} // namespace detail
} // namespace big