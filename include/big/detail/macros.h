#pragma once

// all macros are prefixed with BNI_ which means "big numbers detail"

#ifdef _MSVC_LANG
#define BNI_CXX_VERSION _MSVC_LANG // msvc does not implement the __cplusplus macro properly, unless passed the flag /Zc:__cplusplus
#else
#define BNI_CXX_VERSION __cplusplus
#endif

#if BNI_CXX_VERSION >= 201703L
#define BNI_CONSTEXPR_CXX17 constexpr
#else
#define BNI_CONSTEXPR_CXX17
#endif

#if BNI_CXX_VERSION >= 201402L
#define BNI_CONSTEXPR_CXX14 constexpr
#else
#define BNI_CONSTEXPR_CXX14
#endif