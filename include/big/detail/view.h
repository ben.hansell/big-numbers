#pragma once

// this primitive non-OO interface is used to efficiently implement mathematical operations, agnostic to how memory is allocated and managed
// big::integer wraps this interface and handles memory management

#include <cstdint>
#include <functional>
#include <string> // TODO(ben.hansell): consider removing this dependency for C++ >= 17, or consider using it for all versions, as it implicitly includes <string_view>
#include <vector> // std::vector dependencies will be removed in final version

#include <big/detail/macros.h>
#include <big/detail/part_type.h>
#include <big/detail/util.h>

#if (BNI_CXX_VERSION >= 201703L)
#include <string_view>
#endif

namespace big {
namespace detail { // TODO(ben.hansell): use detail namespace instead of util

#if (BNI_CXX_VERSION >= 201703L)
using string_view_type = const std::string_view; // could be const
#else
using string_view_type = const std::string&; // could be const reference
#endif

struct integer_view {
    const part_type* data;
    std::size_t size;
    part_type back() const;  // most significant part
    part_type front() const; // least significant part
};

// vector-integer_view conversion:
std::vector<part_type> make_vector(integer_view);
integer_view make_view(const std::vector<part_type>&);

// helpers:
bool same(integer_view, integer_view); // true if both views point to the same range of memory
bool sign_of(part_type);
bool sign_of(integer_view);
part_type spread_bool(bool input);
integer_view consolidate(integer_view);
integer_view consolidate_unsigned(integer_view original);
std::function<void(integer_view)> make_writer(std::vector<part_type>*); // creates a functor which populates a given vector with an integer_view
void extend_sign(integer_view, std::size_t new_size, const std::function<void(integer_view)>& callback);
// accesses a part from a view if present, or returns the sign extension if the index is so large:
part_type value_of_part_safe(integer_view, std::size_t index);

void complement(integer_view, const std::function<void(integer_view)>& callback);
void increment_unsigned(integer_view, const std::function<void(integer_view)>& callback);
void shift_left_one_bit(integer_view original, const std::function<void(integer_view)>& callback);
void shift_right_one_bit(integer_view original, const std::function<void(integer_view)>& callback);

bool less_than_unsigned(integer_view, integer_view);
bool greater_than_unsigned(integer_view, integer_view);
bool less_than_or_equal_to_unsigned(integer_view, integer_view);
bool greater_than_or_equal_to_unsigned(integer_view, integer_view);
bool equal_to(integer_view, integer_view);

// mathematical operations:
void unary_minus(integer_view, const std::function<void(integer_view)>& callback);
void add(integer_view, integer_view, const std::function<void(integer_view)>& callback);
void subtract(integer_view, integer_view, const std::function<void(integer_view)>& callback);
void multiply(integer_view, integer_view, const std::function<void(integer_view)>& callback);
void divide(
    integer_view numerator, integer_view denominator, const std::function<void(integer_view)>& result_callback, const std::function<void(integer_view)>& remainder_callback = [](integer_view) {});
void power(integer_view significand, unsigned exponent, const std::function<void(integer_view)>& callback);

// string conversions:
void from_string(string_view_type, const std::function<void(integer_view)>& callback);
std::string to_string(integer_view);

} // namespace detail
} // namespace big