#pragma once

#include <climits>
#include <cstdint>
#include <cstdlib>

#include <big/detail/part_type.h>

// TODO(ben.hansell): consider moving all these to part_type.h

namespace big {
namespace detail {

bool add_overflow(part_type a, part_type b, part_type* result);
bool plus_equals_with_overflow(part_type* a, part_type b);
bool subtract_overflow(part_type a, part_type b, part_type* result);
bool minus_equals_with_overflow(part_type* a, part_type b);
bool check_if_two_most_significant_bits_are_the_same(part_type input); // TODO(ben.hansell): rename to two_most_significant_bits_are_the_same
bool check_if_two_most_significant_bits_are_the_same_portable(part_type input);

#if defined(__GNUC__) && defined(__x86_64__)
bool check_if_two_most_significant_bits_are_the_same_x86_64_gcc(uint64_t);
#endif

} // namespace detail
} // namespace big