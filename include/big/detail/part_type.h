#pragma once

#include <array>
#include <climits>
#include <cstdint>
#include <type_traits>

namespace big {
namespace detail {

// TODO(ben.hansell): determine part_type for compilers which do not provide uintptr_t or where uintptr_t is wider than void*
using part_type = uintptr_t;

constexpr std::size_t part_type_width = sizeof(part_type) * CHAR_BIT;
constexpr std::size_t half_part_type_width = part_type_width / 2;

constexpr part_type negative_one = ~part_type(0); // avoid assuming environment uses twos-complement
constexpr part_type negative_two = negative_one - 1;
constexpr part_type most_positive_value = negative_one >> 1U;
constexpr part_type most_negative_value = most_positive_value + 1;

// an integral type twice the width of part_type, might go unused
class dual_part_type {
  public:
    dual_part_type() = default;
    dual_part_type(part_type small, part_type big);
    dual_part_type(const dual_part_type&) = default;
    dual_part_type(dual_part_type&&) = default;
    dual_part_type& operator=(const dual_part_type&) = default;
    dual_part_type& operator=(dual_part_type&&) = default;
    ~dual_part_type() = default;

  private:
    part_type small;
    part_type big;
    friend bool operator==(dual_part_type, dual_part_type);
    friend dual_part_type operator+(dual_part_type, dual_part_type);
    friend dual_part_type& operator+=(dual_part_type& a, dual_part_type b);
    friend dual_part_type operator<<(dual_part_type, std::size_t);
};

std::array<part_type, 2> multiply_and_expand_unsigned(part_type, part_type);

part_type as_integral(void*);
void* as_pointer(part_type);

} // namespace detail
} // namespace big