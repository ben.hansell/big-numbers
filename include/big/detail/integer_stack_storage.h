#pragma once

#include <big/detail/part_type.h>

namespace big {

class integer;

namespace detail {

class integer_stack_storage {
    static_assert(sizeof(part_type) == sizeof(void*), "the current implementation requires that part_type has the same width as a pointer");
    constexpr static std::size_t number_of_bits = sizeof(void*) * 8;

  public:
    void store_integral(part_type integral);
    void store_pointer(void* pointer);
    part_type load_integral() const;
    void* load_pointer() const;
    bool holds_integral() const;
    bool holds_pointer() const;

  private:
    part_type data{};

  private:
    void mark_stored_value_as_pointer();

    friend class ::big::integer;
};

} // namespace detail
} // namespace big