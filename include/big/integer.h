#pragma once

#include <string>

#include <big/detail/integer_stack_storage.h>
#include <big/detail/macros.h>
#include <big/detail/part_type.h>
#include <big/detail/view.h>

namespace big {

class integer {

  public:
    integer() = default;
    integer(const integer&);
    integer(integer&&) noexcept;
    integer& operator=(const integer&);
    integer& operator=(integer&&) noexcept;
    ~integer();

    // TODO(ben.hansell): consider making these explicit
    integer(const char*);
    integer(const std::string&);
#if BNI_CXX_VERSION >= 201703L
    integer(std::string_view);
#endif

    // TODO(ben.hansell): add constructors from primitive integral types

    friend integer operator+(const integer&, const integer&);
    friend integer operator-(const integer&, const integer&);
    friend integer operator*(const integer&, const integer&);
    friend integer operator/(const integer&, const integer&);
    friend integer operator%(const integer&, const integer&);
    friend bool operator==(const integer&, const integer&);
    friend bool operator!=(const integer&, const integer&);
    friend integer operator-(const integer&);

    friend std::ostream& operator<<(std::ostream&, const integer&);

  private:
    detail::integer_stack_storage stack_storage;

  private: // usable in any state:
    bool heap_allocated() const;
    detail::integer_view get_view() const; // returns a view (works even if !heap_allocated())
    void store(detail::integer_view);      // stores a copy of the given view, doing the necessary reallocations
    void deallocate_maybe();               // calls deallocate() only if memory is owned

    /*
The following only work in the heap-allocated state.
Every function starts with `assert(heap_allocated())`
 */
  private:
    // "capacity" is the number of parts used to store the value plus the number of additional parts which could be stored without need for a reallocation
    std::size_t get_capacity() const;
    void set_capacity(std::size_t);    // sets value blindly
    void change_capacity(std::size_t); // resizes array, checks that new capacity >= size

    // "size" is the number of parts use (and no more than needed) to store the value
    std::size_t get_size() const;
    void set_size(std::size_t);             // sets value blindly
    void change_size(std::size_t);          // manages memory if necessary
    const detail::part_type* parts() const; // const pointer to least significant part
    detail::part_type* parts();             // pointer to least significant part
    void deallocate();                      // changes to SBO state with any valid value

    /*
The following only work in the SBO state.
Every function starts with `assert(!heap_allocated())`
 */
  private:
    detail::part_type load_sbo_value() const;
    void set_sbo_value(detail::part_type);     // sets value blindly
    bool try_set_sbo_value(detail::part_type); // sets value if possible
    void allocate(std::size_t size);           // changes to heap-allocated state from sbo state, value left undefined
};

namespace literals {

// TODO(ben.hansell): this operator should support C++14 single-quote separators
// TODO(ben.hansell): this operaror should reject invalid strings at compile time
integer operator"" _bi(const char* as_text);

} // namespace literals

} // namespace big