# Big Numbers

WARNING:  This library is not finished yet.  Bugs and API changes may appear on master at any time.

## Features

- Two's complement
- Small buffer optimisation (for values between -2^62 and 2^62-1)[^assuming-64-bit]
- Small stack footprint, equal to that of a *single pointer*, i.e. 8 bytes[^assuming-64-bit]

## `big::integer` usage

`big::integer` is an integer which has an unlimited range.  Other than that it behaves exactly like a normal `int`, e.g:

```C++
#include <big/integer.h>
#include <iostream>

using namespace big::literals;

int main () {
    big::integer a = 65536_bi;
    a *= a;
    std::cout << a << std::endl; // prints "4294967296"
    a = -a;
    std::cout << a << std::endl; // prints "-4294967296"
    a *= a;
    std::cout << a << std::endl; // prints "18446744073709551616"
    a *= a;
    std::cout << a << std::endl; // prints "340282366920938463463374607431768211456"
}
```

Numbers which are too large to be represented by an `int`, can be constructed via a string, e.g:

``` C++
big::integer  b("100000000000000000000000000000000000000000");
```

They can also be constructed with a literal, e.g.:

```C++
using namespace big::literals;
auto b = 100000000000000000000000000000000000000000_bi
```

<!---
## `big::quotient`

`big::quotient` stores two `big::integer`s, a numerator and a denominator.  It can represent any rational number.

```C++
#include <big/quotient.h>
#include <iostream>

int main () {
    big::quotient a = 68;
    big::quotient b = a / 60; // c is stored precisely as 17 / 15
    std::cout << std::setprecision(12) << c << std::endl; // prints "1.133333333333"
    std::cout << c.to_string_recurring() << std::endl; // prints "1.1`3"
    std::cout << c.to_string_quotient() << std::endl; // prints "17/15
}
```

`big::quotient` can also be constructed from a string or a literal, e.g.

``` C++
big::integer a = "68/60";
auto b = "68/60"_bq;
auto c = "1.1`3"_bq; // note the special character is a back tick, not a single quote 
auto d = 68_bq / 60_bq;
```

## `big::decimal`

`big::decimal` is a base 10 floating point type, where the significand and exponent are both `big::integer`s (or the implementation will be equivalent).

```C++
#include <big/decimal.h>
#include <iostream>

int main () {
    big::decimal a = 65536.2;
    a /= 10;
    std::cout << a << std::endl; // prints "6553.62"
}
```

**Warning:** Converting from a particularly large `big::decimal` to a `big::integer` may result in a huge memory allocation which may crash your programme.

## `big::quotient_decimal`

`big::quotient` is a type which effectively stores three `big::integer`s (a numerator, denominator and exponent).  Unlike typical quotient implementations, the exponent allows really large (or small) values such as (2 / 3) * 10^1000 to be stored without using huge amounts of memory.  This means you can efficiently convert any well-formed `big::decimal` to a `big::quotient` without the chance of a huge memory allocation.

Note to self:  it might be better for big::quotient to not have an exponent.  Then it would not be susceptible to The `big::decimal` addition problem (discussed below).  A further `big::quotient_decimal` could later be implemented if there is sufficient demand.
-->

## Compiler Support

Requires C++11 or newer (but provides enhanced API and performance for C++14 and C++17).
  
### Linux x86

CI is used to test every commit with Clang and GCC, on 32 bit and 64 bit x86.

### Windows & Mac x86

Anecdotally, the library has built using MSVC on Windows and Apple Clang on OSX.  Some workarounds have been added where these compilers do not strictly conform to the C++ standard.  However, the CI is not currently configured to ensure compatibility on every commit.  On Apple Clang, C++17 is currently required, though that could be fixed in future.

### Other

Standard-conformant C++17 compilers should work[^pragma-once-required].  But, you should run unit tests to make sure.  If you find a problem, please report it.  C++11/14 support currently depends on common non-standard extensions, though that could be fixed in future.

[^pragma-once-required]:  Technically, the non-standard `#pragma once` directive is used.

## Installation

The project is built with CMake.  Conan support is planned in the future.

<!---
For convenience, a quick-start **single header** file, containing all the same functionality, is also available on the releases page.
-->

<!--- 
## STL compatibility

### `std::hash`

`std::hash` is implemented so that e.g. `std::unordered_set` can be used

### `std::optional`

Unlike IEEE 754, none of the types in this library can hold `NaN` or `±inf`.  For that reason, on some systems, `std::optional` is overloaded for space efficiency.  E.g. on x86_64 `sizeof(big::integer) == 8` and `sizeof(std::optional<big::integer>) == 8`.  Similarly `sizeof(big::decimal) == 16` and `sizeof(std::optional<big::decimal>) == 16`

### \<cmath\> Functions

RNG? 

-->

## Thread safety

None of the types are thread-safe.  Simultaneous read-write or write-write can lead to segmentations faults.

## Optimisations

### SBO

Small-buffer optimisation means that any integer value in the range - 2^62 <= x < 2^62 will be stored on the without dynamic memory allocation[^assuming-64-bit].

[^assuming-64-bit]: Assuming 64-bit architecture.

Despite the SBO, the stack footprint of `big::integer` is just 64 bits[^assuming-64-bit].  This is achieved using compressed pointers.

<!---
Some arithmetic operations are also optimised for the small value case.  For instance, addition of two small `big::integer`s first tries to perform the calculation in place, then falls back to a the generic implementation if the small buffer overflows.
-->

<!---
### CoW

Copy-on-write optimisation is used, for example, by `big::integer` when SBO is not possible.  This may save a lot of memory when dealing with extremely large integers, but also results in a speed boost for quite large integers.
-->

### Assembly Optimisations

For some platforms, a few performance-critical routines are provided in hand-written inline assembly.  This shouldn't interfere with compilers which do not support inline assembly.

### Move Semantics

Move semantics are implemented, so the normal things work as expected:

``` C++
using namespace big::literals
big::integer a = 340282366920938463463374607431768211456_bi; // heap allocation
big::integer b = a; // heap allocation
big::integer c = std::move(a); // no memory allocation, but the value of a is now undefined
```
<!---
This might seem unnecessary given that CoW is implemented, but consider the following:

```C++
big::integer a = "340282366920938463463374607431768211456"; // heap allocation
++a; // no heap allocation
big::integer b = a; // no heap allocation thanks to CoW
// a and b now share the same heap space and cannot be mutated without a reallocation
++b; // heap allocation
```

The above code would have required one less heap allocation if the move assignment `big::integer b = std::move(a);` had been used.
-->

<!---
Additional atypical move semantics are also available in this library, such as:

``` C++
big::integer a = "340282366920938463463374607431768211456"; // heap allocation
big::integer b = 3; // heap allocation
big::integer c = a + b; // heap allocation, a and b hold their original values
big::integer d = std::move(a) + std::move(b); // no heap allocation, the value of d is equal to c, as expected, but the values of a and b may have changed
```
-->

<!---
## Limitations

### Irrational Numbers

Irrational numbers, such as π, cannot be represented precisely by any of these types.

### Mixing `big::integer` and `big::decimal`

The usual caveats of mixing `int`s and `float`s in C++ also apply to `big::integer` and `big::decimal` (and `big::quotient`).  E.g:

```C++
big::integer c = 7;
big::integer d = 2;
big::decimal e = c / d; // 3
```

The above produces 3 (not 3.5 as you might expect) because the division is done using integer arithmetic, and then the integer result is converted to a decimal, instead you can do 

```C++
big::integer c = 7;
big::integer d = 2;
big::decimal e = big::decimal(c) / d; // 3.5
```

or for maximum efficiency

``` C++
big::integer c = 7;
big::integer d = 2;
big::decimal e = big::divide<big::decimal>(c, d); // 3.5
```

The above is all true if you substitute `big::quotient` in place of `big::decimal`.  For `big::quotient`, there is also the efficient option of using the constructor:

``` C++
big::integer c = 7;
big::integer d = 2;
big::decimal e = big::quotient(c, d); // 3.5
```

### The `big::decimal` Addition Problem

`big::decimal` can be used for representing extremely large and small numbers and is conceptually similar to standard form.  Imagine precisely adding the two standard form numbers (with a pencil and paper)

```
1 × 10 ^ 50 + 1 × 10 ^ -50
```

The answer would be something like

```
10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001 × 10 ^ -50
```

You can see that the significand becomes unwieldy.  Now imagine that instead of `50` and `-50`, you have `50000` and `-50000`.  The significand would become so unwieldy that you could not hope to write the answer on a piece of paper.  For the same reason, the following code would result in an extremely large memory allocation, and might cause the program to crash:

``` C++
big::decimal a("4e1000000000000");
a++; // likely program crash
```

Normal `float`s and `double`s do not have this issue because they have a limited significand precision.  Calculations such as the above would produce inexact results, but no crash.  The whole point of `big::decimal` is that it does not limit precision, therefore you must be careful not to do calculations such as the above.

-->

## Footnotes

<!--- footnotes should appear here, with a compatible Markdown renderer -->