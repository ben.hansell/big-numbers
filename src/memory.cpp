#include <big/detail/memory.h>

#include <cassert>
#include <cstdint>

#if defined(_MSC_VER)
#include <malloc.h>
#elif __cplusplus >= 201703L
#include <cstdlib>
#else
#include <stdlib.h> // TODO(#6): implement a generic aligned_alloc for unrecognised platforms
#endif

#include <big/detail/part_type.h>

namespace big {
namespace detail {

namespace {

bool is_aligned(std::size_t alignment, void* ptr) {
    return (as_integral(ptr) / alignment) * alignment == as_integral(ptr);
}

std::size_t round_up(std::size_t input, std::size_t multiple) {
    assert(multiple != 0);
    return ((input + multiple - 1) / multiple) * multiple;
}

} // namespace

void* aligned_alloc_2(std::size_t size) {
    // std::aligned_alloc is not guaranteed to work with all alignments and all sizes
    constexpr std::size_t alignment = sizeof(void*);            // this is an alignment which is valid in *most* environments
#if __cplusplus >= 201703L && !defined(__apple_build_version__) // Apple Clang 12 does not seem to have std::aligned_alloc, even in C++17 mode
    return std::aligned_alloc(alignment, round_up(size, alignment));
#elif defined(_MSC_VER) // TODO: check if MSVC supports std::aligned_alloc in C++17 mode and beware that MSVC misuses the __cplusplus macro
    return _aligned_malloc(size, 2);
#else
    return ::aligned_alloc(alignment, round_up(size, alignment));
#endif
}

void aligned_free_2(void* ptr) {
    assert(is_aligned(2, ptr));
#if defined(_MSC_VER)
    _aligned_free(ptr);
#else
    std::free(ptr);
#endif
}

} // namespace detail
} // namespace big