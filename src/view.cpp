#include <big/detail/view.h>

#include <algorithm>
#include <bitset>
#include <cassert>
#include <stdexcept>

namespace big {
namespace detail {

part_type integer_view::front() const {
    return data[0];
}

part_type integer_view::back() const {
    return data[size - 1];
}

integer_view make_view(const std::vector<part_type>& source) {
    return {source.data(), source.size()};
}

std::vector<part_type> make_vector(integer_view source) {
    return std::vector<part_type>(source.data, source.data + source.size);
}

bool same(integer_view a, integer_view b) {
    return a.data == b.data && a.size == b.size;
}

bool sign_of(part_type val) {
    return std::bitset<part_type_width>(val)[part_type_width - 1];
}

bool sign_of(integer_view val) {
    return sign_of(val.back());
}

part_type spread_bool(bool input) {
    return input ? negative_one : 0;
}

integer_view consolidate(integer_view original) {
    if (original.size == 1) {
        return original;
    } else if (original.back() == 0 && !sign_of(original.data[original.size - 2])) {
        return consolidate({original.data, original.size - 1});
    } else if (original.back() == negative_one && sign_of(original.data[original.size - 2])) {
        return consolidate({original.data, original.size - 1});
    } else {
        return original;
    }
}

// not unit tested, yet
integer_view consolidate_unsigned(integer_view original) {
    if (original.size == 1) {
        return original;
    } else if (original.back() == 0) {
        return consolidate_unsigned({original.data, original.size - 1});
    } else {
        return original;
    }
}

std::function<void(integer_view)> make_writer(std::vector<part_type>* destination) {
    return [=](integer_view source) { *destination = std::vector<part_type>(source.data, source.data + source.size); };
}

void extend_sign(integer_view original, std::size_t new_size, const std::function<void(integer_view)>& callback) {
    // in future this will be implemented using alloca rather than std::vector for efficiency
    assert(new_size >= original.size);
    std::vector<part_type> result = make_vector(original);
    const part_type number_of_additional_parts = new_size - original.size;
    const part_type state_of_additional_parts = spread_bool(sign_of(original)); // all bits set or all bits clear
    for (std::size_t i = 0; i < number_of_additional_parts; i++) {
        result.push_back(state_of_additional_parts);
    }
    callback(make_view(result));
}

// not unit tested, yet
part_type value_of_part_safe(integer_view original, std::size_t index) {
    if (index < original.size) {
        return original.data[index];
    } else {
        return spread_bool(sign_of(original));
    }
}

void complement(integer_view original, const std::function<void(integer_view)>& callback) {
    std::vector<part_type> result;
    for (std::size_t i = 0; i != original.size; i++) {
        result.push_back(~original.data[i]);
    }
    callback(make_view(result));
}

// not in header
bool every_bit_is_set(integer_view x) {
    for (std::size_t i = 0; i < x.size; i++) {
        if (x.data[i] != negative_one) {
            return false;
        }
    }
    return true;
}

// not in header
void increment_unsigned_with_overflow(integer_view x, const std::function<void(integer_view)>& callback) {
    std::vector<part_type> result(x.data, x.data + x.size);
    for (part_type& part : result) {
        part++;
        if (part != 0) {
            break;
        }
    }
    callback(make_view(result));
}

void increment_unsigned(integer_view x, const std::function<void(integer_view)>& callback) {
    if (every_bit_is_set(x)) { // this case could be generalised into the default case
        std::vector<part_type> result(x.size + 1, 0);
        result.back() = 1;
        callback(make_view(result));
    } else {
        increment_unsigned_with_overflow(x, callback);
    }
}

void shift_left_one_bit(integer_view original, const std::function<void(integer_view)>& callback) {
    std::vector<part_type> result = make_vector(original);
    result.push_back(0);
    bool previous_carry = false;
    for (part_type& part : result) {
        bool next_carry = bool(part >> (part_type_width - 1U));
        part <<= 1U;
        part |= part_type(previous_carry);
        previous_carry = next_carry;
    }
    // TODO(ben.hansell) : consider this alternative
    /*
    if (result.back() == 0) {
        return callback({result.data(), result.size() - 1});
    } else {
        return callback({result.data(), result.size()});
    }*/
    callback(consolidate_unsigned(make_view(result)));
}

// takes in a positive value and returns a positive value
void shift_right_one_bit(integer_view original, const std::function<void(integer_view)>& callback) {
    // treats original as unsigned
    std::vector<part_type> result = make_vector(original);
    bool previous_carry = false;
    for (auto rit = result.rbegin(); rit != result.rend(); ++rit) {
        part_type& part = *rit;
        bool next_carry = bool(part & part_type(1));
        part >>= 1U;
        part |= (part_type(previous_carry) << (part_type_width - 1));
        previous_carry = next_carry;
    }
    callback(consolidate(make_view(result))); // to do: consider effect of signed consolidation
}

bool less_than_unsigned(integer_view a, integer_view b) {
    a = consolidate_unsigned(a);
    b = consolidate_unsigned(b);
    if (a.size > b.size) {
        return false;
    } else if (b.size > a.size) {
        return true;
    }
    for (std::size_t i = 0; i < a.size; i++) {
        std::size_t idx = a.size - 1 - i;
        if (a.data[idx] < b.data[idx]) {
            return true;
        } else if (a.data[idx] > b.data[idx]) {
            return false;
        }
    }
    return false;
}

bool greater_than_unsigned(integer_view a, integer_view b) {
    return less_than_unsigned(b, a);
}

bool less_than_or_equal_to_unsigned(integer_view a, integer_view b) {
    return !greater_than_unsigned(a, b);
}

bool greater_than_or_equal_to_unsigned(integer_view a, integer_view b) {
    return !less_than_unsigned(a, b);
}

bool equal_to(integer_view a, integer_view b) {
    if (a.size != b.size) {
        return false;
    }
    for (std::size_t i = 0; i < a.size; i++) {
        if (a.data[i] != b.data[i]) {
            return false;
        }
    }
    return true;
}

// not in header
bool is_most_negative_possible_value_for_width(integer_view x) {
    if (x.back() != most_negative_value) {
        return false;
    }
    for (std::size_t i = 0; i < x.size - 1; i++) { // x.size > 0 assumed
        if (x.data[i] != 0) {
            return false;
        }
    }
    return true;
}

void unary_minus(integer_view original, const std::function<void(integer_view)>& callback) {
    if (is_most_negative_possible_value_for_width(original)) {
        std::vector<part_type> result(original.data, original.data + original.size);
        result.push_back(0);
        callback(make_view(result));
    } else {
        complement(original, [&callback](integer_view comp) { increment_unsigned_with_overflow(comp, [&callback](integer_view m) { callback(consolidate(m)); }); });
    }
}

void add(integer_view a, integer_view b, const std::function<void(integer_view)>& callback) {
    std::size_t max_size_of_result = std::max(a.size, b.size) + 1;
    std::vector<part_type> result(max_size_of_result);
    part_type overflow = 0;
    for (std::size_t i = 0; i < result.size(); i++) {
        overflow = static_cast<part_type>(add_overflow(value_of_part_safe(a, i), overflow, &result[i]));
        overflow += static_cast<part_type>(add_overflow(result[i], value_of_part_safe(b, i), &result[i]));
    }
    callback(consolidate(make_view(result)));
}

void subtract(integer_view minuend, integer_view subtrahend, const std::function<void(integer_view)>& callback) {
    std::size_t max_size_of_result = std::max(subtrahend.size, minuend.size) + 1;
    std::vector<part_type> result(max_size_of_result);
    part_type overflow = 0; // TODO(#2): could be bool or part_type (benchmark needed)
    for (std::size_t i = 0; i < result.size(); i++) {
        overflow = static_cast<part_type>(subtract_overflow(value_of_part_safe(minuend, i), overflow, &result[i]));
        overflow += static_cast<part_type>(subtract_overflow(result[i], value_of_part_safe(subtrahend, i), &result[i]));
    }
    callback(consolidate(make_view(result)));
}

void multiply(integer_view a, integer_view b, const std::function<void(integer_view)>& callback) {
    auto multiply_two_positive_operands = [](integer_view a, integer_view b, const std::function<void(integer_view)>& callback) {
        std::vector<part_type> result = {0};
        for (std::size_t i = 0; i < a.size; i++) {
            for (std::size_t j = 0; j < b.size; j++) {
                std::array<part_type, 2> product_of_parts = multiply_and_expand_unsigned(a.data[i], b.data[j]);
                std::vector<part_type> product_of_parts_shifted(i + j, 0u);
                product_of_parts_shifted.push_back(product_of_parts[0]);
                product_of_parts_shifted.push_back(product_of_parts[1]);
                add(make_view(result), make_view(product_of_parts_shifted), make_writer(&result));
            }
        }
        callback(make_view(result));
    };
    if (!sign_of(a) && !sign_of(b)) { // both are positive
        multiply_two_positive_operands(a, b, callback);
    } else if (sign_of(a) && sign_of(b)) { // both are negative
        unary_minus(a, [&](integer_view minus_a) { unary_minus(b, [&](integer_view minus_b) { multiply_two_positive_operands(minus_a, minus_b, callback); }); });

    } else { // one is negative and one is positive
        bool a_is_positive = !sign_of(a);
        integer_view positive_operand = a_is_positive ? a : b;
        integer_view negative_operand = a_is_positive ? b : a;
        unary_minus(negative_operand, [&](integer_view minus_negative_operand) { multiply_two_positive_operands(positive_operand, minus_negative_operand, [&callback](integer_view i) { unary_minus(i, callback); }); });
    }
}

// not in headder
void divide_unsigned(integer_view numerator, integer_view denominator, const std::function<void(integer_view)>& result_callback, const std::function<void(integer_view)>& remainder_callback) {
    // significant room for optimisation
    std::vector<part_type> remainder = make_vector(numerator);
    std::vector<part_type> result(numerator.size, 0);
    auto set_result_bit = [&result](std::size_t idx) {
        part_type& part = result[idx / part_type_width];
        part_type set_bit = part_type(1) << (idx % part_type_width);
        part |= set_bit;
    };
    while (greater_than_or_equal_to_unsigned(make_view(remainder), denominator)) {
        std::vector<part_type> denominator_shifted_left = make_vector(denominator);
        std::size_t number_of_bits_shifted = 0;
        while (less_than_or_equal_to_unsigned(make_view(denominator_shifted_left), make_view(remainder))) {
            shift_left_one_bit(make_view(denominator_shifted_left), make_writer(&denominator_shifted_left));
            number_of_bits_shifted++;
        }
        shift_right_one_bit(make_view(denominator_shifted_left), make_writer(&denominator_shifted_left));
        number_of_bits_shifted--;
        set_result_bit(number_of_bits_shifted);
        assert(less_than_or_equal_to_unsigned(make_view(denominator_shifted_left), consolidate(make_view(remainder))));
        subtract(make_view(remainder), consolidate(make_view(denominator_shifted_left)), make_writer(&remainder));
        assert(greater_than_or_equal_to_unsigned(make_view(remainder), make_view({0})));
    }
    result_callback(consolidate(make_view(result)));
    remainder_callback(consolidate(make_view(remainder)));
}

/*
TODO(ben.hansell)
"If both operands are nonnegative then the remainder is nonnegative; if not, the sign of the remainder is implementation-defined."
from ISO14882:2003(e) is no longer present in ISO14882:2011(e)
The remainder in this case will be abs(numberator) % abs(denominator) check whether that conforms to C++11
look into the result of x%0
*/
void divide(integer_view numerator, integer_view denominator, const std::function<void(integer_view)>& result_callback, const std::function<void(integer_view)>& remainder_callback) {
    if (!sign_of(numerator) && !sign_of(denominator)) { // both are positive
        divide_unsigned(numerator, denominator, result_callback, remainder_callback);
    } else if (sign_of(numerator) && sign_of(denominator)) { // both are negative
        unary_minus(numerator, [&](integer_view minus_numerator) { unary_minus(denominator, [&](integer_view minus_denominator) { divide_unsigned(minus_numerator, minus_denominator, result_callback, [&](integer_view remainder) { unary_minus(remainder, remainder_callback); }); }); });
    } else if (sign_of(numerator)) { // numerator is negative
        unary_minus(numerator, [&](integer_view minus_numerator) {
            divide_unsigned(
                minus_numerator, denominator, [&](integer_view minus_result) { unary_minus(minus_result, result_callback); }, [&](integer_view remainder) { unary_minus(remainder, remainder_callback); });
        });
    } else { // denominator is negative
        unary_minus(denominator, [&](integer_view minus_denominator) {
            divide_unsigned(
                numerator, minus_denominator, [&](integer_view minus_result) { unary_minus(minus_result, result_callback); }, remainder_callback);
        });
    }
}

void power(integer_view significand, unsigned exponent, const std::function<void(integer_view)>& callback) {
    // faster algo could be used where value is squared and squared again as many times as necessary etc
    std::vector<part_type> result = {1U};
    for (unsigned i = 0; i < exponent; i++) {
        multiply(make_view(result), significand, make_writer(&result));
    }
    callback(make_view(result));
}

void from_string_unsigned(string_view_type source, const std::function<void(integer_view)>& callback) {
    if (source.empty()) {
        throw std::invalid_argument("cannot convert string \"-\" to big::integer");
    }
    if (!std::all_of(source.begin(), source.end(), ::isdigit)) {
        throw std::invalid_argument("cannot convert string containing non-digit character to big::integer");
    }
    std::vector<part_type> result = {0U};
    unsigned ridx = 0;
    constexpr part_type ten = 10U;
    for (auto rit = source.rbegin(); rit != source.rend(); ++rit, ++ridx) {
        const part_type digit = *rit - 48;
        power({&ten, 1}, ridx, [&](integer_view order_of_magnitude) { multiply({&digit, 1}, order_of_magnitude, [&](integer_view digit_with_trailing_zeroes) { add(make_view(result), digit_with_trailing_zeroes, make_writer(&result)); }); });
    }
    callback(make_view(result));
}

void from_string(string_view_type source, const std::function<void(integer_view)>& callback) {
    if (source.empty()) {
        throw std::invalid_argument("cannot convert empty string to big::integer");
    }
    if (source[0] == '-') {
        from_string_unsigned({&source[0] + 1, source.size() - 1}, [&callback](integer_view magnitude) { unary_minus(magnitude, callback); });
    } else {
        from_string_unsigned(source, callback);
    }
}

std::string to_string_positive(integer_view value) {
    const std::vector<part_type> ten = {10};
    std::string result;
    divide(
        value, make_view(ten),
        [&result](integer_view result_of_division) {
            if (result_of_division.size == 1 && result_of_division.data[0] == 0) {
            } else {
                result = to_string_positive(result_of_division) + result;
            }
        },
        [&result](integer_view remainder) { result = result + std::to_string(remainder.data[0]); });
    return result;
}

std::string to_string(integer_view value) {
    if (sign_of(value)) {
        std::string result = "-";
        unary_minus(value, [&result](integer_view minus_value) { result += to_string_positive(minus_value); });
        return result;
    } else {
        return to_string_positive(value);
    }
}

} // namespace detail
} // namespace big