#include <big/integer.h>

#include <cassert>
#include <cstddef>
#include <cstring>
#include <iostream>

#include <big/detail/memory.h>
#include <big/detail/part_type.h>
#include <big/detail/util.h>

using big::detail::part_type;

namespace {

std::size_t required_allocation_size(std::size_t number_of_parts) {
    return number_of_parts * sizeof(part_type) + 2 * sizeof(std::size_t);
}

} // namespace

namespace big {

integer::integer(const integer& other) {
    if (other.heap_allocated()) {
        store(other.get_view());
    } else {
        stack_storage.store_integral(other.stack_storage.load_integral());
    }
}

integer::integer(integer&& other) noexcept {
    stack_storage.data = other.stack_storage.data;
    other.stack_storage.store_integral(123456);
}

integer& integer::operator=(const integer& other) {
    if (other.heap_allocated()) {
        if (&other == this) {
            return *this;
        }
        store(other.get_view());
    } else {
        deallocate_maybe();
        stack_storage.store_integral(other.stack_storage.load_integral());
    }
    return *this;
}

integer& integer::operator=(integer&& other) noexcept {
    std::swap(stack_storage.data, other.stack_storage.data);
    return *this;
}

integer::~integer() {
    deallocate_maybe();
}

integer::integer(const char* as_text)
    : integer(detail::string_view_type(as_text)) {
}

integer::integer(const std::string& as_text) {
    detail::from_string(as_text, [this](detail::integer_view view) { store(view); });
}

#if BNI_CXX_VERSION >= 201703L
integer::integer(std::string_view as_text) {
    detail::from_string(as_text, [this](detail::integer_view view) { store(view); });
}
#endif

integer operator+(const integer& a, const integer& b) {
    integer result;
    detail::add(a.get_view(), b.get_view(), [&result](detail::integer_view view_of_result) { result.store(view_of_result); });
    return result;
}

integer operator-(const integer& a, const integer& b) {
    integer result;
    detail::subtract(a.get_view(), b.get_view(), [&result](detail::integer_view view_of_result) { result.store(view_of_result); });
    return result;
}

integer operator*(const integer& a, const integer& b) {
    integer result;
    detail::multiply(a.get_view(), b.get_view(), [&result](detail::integer_view view_of_result) { result.store(view_of_result); });
    return result;
}

integer operator/(const integer& a, const integer& b) {
    integer result;
    detail::divide(a.get_view(), b.get_view(), [&result](detail::integer_view view_of_result) { result.store(view_of_result); });
    return result;
}

integer operator%(const integer& a, const integer& b) {
    integer result;
    detail::divide(
        a.get_view(), b.get_view(), [](detail::integer_view) {}, [&result](detail::integer_view view_of_result) { result.store(view_of_result); });
    return result;
}

bool operator==(const integer& a, const integer& b) {
    return detail::equal_to(a.get_view(), b.get_view());
}

bool operator!=(const integer& a, const integer& b) {
    return !(a == b);
}

integer operator-(const integer& x) {
    integer result;
    detail::unary_minus(x.get_view(), [&result](detail::integer_view view_of_result) { result.store(view_of_result); });
    return result;
}

bool integer::heap_allocated() const {
    return stack_storage.holds_pointer();
}

detail::integer_view integer::get_view() const {
    if (heap_allocated()) {
        return {parts(), get_size()};
    } else {
        return {&stack_storage.data, 1};
    }
}

void integer::store(const detail::integer_view view) {
    // TODO(ben.hansell): reuse already-allocated space when possible
    // TODO(ben.hansell): assert that view cannot be compacted
    // TODO(ben.hansell): consider if a view of size zero is possible
    deallocate_maybe(); // TODO(ben.hansell): optimise this by reallocating rather than deallocating sometimes
    bool sbo = view.size == 1 && detail::check_if_two_most_significant_bits_are_the_same(view.front());
    if (sbo) {
        stack_storage.store_integral(view.front());
    } else {
        void* new_memory = detail::aligned_alloc_2(required_allocation_size(view.size));
        std::size_t& capacity_reference = *reinterpret_cast<std::size_t*>(new_memory);
        std::size_t& size_reference = *(reinterpret_cast<std::size_t*>(new_memory) + 1);
        capacity_reference = view.size;
        size_reference = view.size;
        part_type* parts = reinterpret_cast<part_type*>(reinterpret_cast<std::size_t*>(new_memory) + 2);
        std::copy(view.data, view.data + view.size, parts);
        stack_storage.store_pointer(new_memory);
    }
}

void integer::deallocate_maybe() {
    if (heap_allocated()) {
        deallocate();
    }
}

std::size_t integer::get_size() const {
    assert(heap_allocated());
    return *(reinterpret_cast<std::size_t*>(stack_storage.load_pointer()) + 1);
}

const part_type* integer::parts() const {
    return reinterpret_cast<const part_type*>(reinterpret_cast<const std::size_t*>(stack_storage.load_pointer()) + 2);
}

void integer::deallocate() {
    assert(heap_allocated());
    detail::aligned_free_2(stack_storage.load_pointer());
    stack_storage.store_integral(1234567); // recognisable sequence used to remind user that deallocate() does not set value to 0
}

std::ostream& operator<<(std::ostream& os, const integer& x) {
    os << detail::to_string(x.get_view());
    return os;
}

namespace literals {

integer operator"" _bi(const char* as_text) {
    return integer(as_text);
}

} // namespace literals

} // namespace big