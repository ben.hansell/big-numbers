#include <big/detail/part_type.h>

#include <climits>
#include <tuple>

#include <big/detail/util.h>

#if defined(_MSC_VER) && defined(_WIN64) // Visual Studio 64-bit
#include <intrin.h>
#pragma intrinsic(_umul128)
#endif

namespace big {
namespace detail {

dual_part_type::dual_part_type(part_type small, part_type big)
    : small(small)
    , big(big) {}

bool operator==(dual_part_type a, dual_part_type b) {
    return std::tie(a.small, a.big) == std::tie(b.small, b.big);
}

dual_part_type operator+(dual_part_type a, dual_part_type b) {
    dual_part_type result;
    result.big = a.big + b.big;
    result.big += (part_type)add_overflow(a.small, b.small, &result.small);
    return result;
}

dual_part_type& operator+=(dual_part_type& a, dual_part_type b) {
    a = a + b;
    return a;
}

dual_part_type operator<<(const dual_part_type original, const std::size_t number_of_bits) {
    // usually shifting by a number of bits which is >= the width of the type or < 0 is undefined
    auto shift_left_general = [](part_type value, int number_of_bits) -> part_type {
        if ((unsigned)std::abs(number_of_bits) >= part_type_width) {
            return 0;
        }
        if (number_of_bits > 0) {
            return value << number_of_bits; // NOLINT
        } else {
            return value >> -number_of_bits; // NOLINT
        }
    };

    dual_part_type result;
    result.big = shift_left_general(original.big, number_of_bits);
    result.small = shift_left_general(original.small, number_of_bits);

    result.big |= shift_left_general(original.small, int(number_of_bits) - part_type_width);
    return result;
}

// warning: only one version of multiply_and_expand_unsigned_impl is ever seen by unit tests
// TODO(ben.hansell): add specialisations for uint32_t and uint16_t
template <typename part_type>
std::array<part_type, 2> multiply_and_expand_unsigned_impl(const part_type a, const part_type b) {
    constexpr std::size_t half_part_type_width = sizeof(part_type) * CHAR_BIT / 2;
    const part_type a0 = a << half_part_type_width >> half_part_type_width;
    const part_type b0 = b << half_part_type_width >> half_part_type_width;
    const part_type a1 = a >> half_part_type_width;
    const part_type b1 = b >> half_part_type_width;

    const part_type p00 = a0 * b0;
    const part_type p01 = a0 * b1;
    const part_type p10 = a1 * b0;
    const part_type p11 = a1 * b1;

    const part_type middle = p10 + (p00 >> half_part_type_width) + (p01 << half_part_type_width >> half_part_type_width);

    const part_type higher_part = p11 + (middle >> half_part_type_width) + (p01 >> half_part_type_width);
    const part_type lower_part = a * b;

    return {lower_part, higher_part};
}

#ifdef __SIZEOF_INT128__
template <>
std::array<uint64_t, 2> multiply_and_expand_unsigned_impl(const uint64_t a, const uint64_t b) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
    const unsigned __int128 intermediate = (unsigned __int128)(a) * (unsigned __int128)(b);
#pragma GCC diagnostic pop
    const std::array<uint64_t, 2> result = {uint64_t(intermediate), uint64_t(intermediate >> 64U)};
    return result;
}
#elif defined(_MSC_VER) && defined(_WIN64)
template <>
std::array<uint64_t, 2> multiply_and_expand_unsigned_impl(const uint64_t a, const uint64_t b) {
    std::array<uint64_t, 2> result;
    result[0] = _umul128(a, b, &result[1]);
    return result;
}
#endif

std::array<part_type, 2> multiply_and_expand_unsigned(part_type a, part_type b) {
    return multiply_and_expand_unsigned_impl(a, b);
}

// TODO(ben.hansell): find a way of making these functions constexpr
part_type as_integral(void* pointer) {
    return reinterpret_cast<part_type>(pointer);
}

void* as_pointer(part_type integral) {
    return reinterpret_cast<void*>(integral);
}

} // namespace detail
} // namespace big