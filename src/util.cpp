#include <big/detail/util.h>

namespace big {
namespace detail {

// a portable drop in replacement for __builtin_add_overflow which optimises down to the same assembly in Clang and ICC and better assembly in GCC
bool add_overflow(const part_type a, const part_type b, part_type* result) {
    *result = a + b;
    return *result < a; // could also use b
}

bool plus_equals_with_overflow(part_type* a, part_type b) {
    return add_overflow(*a, b, a);
}

bool subtract_overflow(part_type a, part_type b, part_type* result) {
    *result = a - b;
    return *result > a;
}

// not unit tested, yet
bool minus_equals_with_overflow(part_type* a, part_type b) {
    return subtract_overflow(*a, b, a);
}

bool check_if_two_most_significant_bits_are_the_same(part_type input) {
#if defined(__GNUC__) && defined(__x86_64__)
    static_assert(sizeof(part_type) == sizeof(uint64_t), "");
    return check_if_two_most_significant_bits_are_the_same_x86_64_gcc(input);
#else
    return check_if_two_most_significant_bits_are_the_same_portable(input);
#endif
}

bool check_if_two_most_significant_bits_are_the_same_portable(part_type input) {
    input ^= input << 1U;
    return !static_cast<bool>(input >> (part_type_width - 1));
}

// TODO(ben.hansell): so far, no benchmarks show that this measurably improves performace. Investigate that, and remove this optimisation if it is useless
#if defined(__GNUC__) && defined(__x86_64__) // should be true for GCC and Clang
bool check_if_two_most_significant_bits_are_the_same_x86_64_gcc(uint64_t input) {
    bool result;
    asm("shrq $62, %1\n"
        "setp %0"
        : "=r"(result)
        : "r"(input)
        :);
    return result;
}
#endif

} // namespace detail
} // namespace big