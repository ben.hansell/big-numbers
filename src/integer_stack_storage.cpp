#include <big/detail/integer_stack_storage.h>

#include <cassert>

#include "big/detail/util.h"

namespace big {
namespace detail {

namespace {
constexpr bool lsb_is_clear(const part_type value) {
    return value == (value >> 1) << 1;
}
} // namespace

void integer_stack_storage::store_integral(part_type integral) {
    data = integral;
    assert(holds_integral());
}

void integer_stack_storage::store_pointer(void* pointer) {
    assert(lsb_is_clear(as_integral(pointer))); // least-significant bit must be 0
    data = as_integral(pointer) >> 1;
    mark_stored_value_as_pointer();
    assert(holds_pointer());
}

part_type integer_stack_storage::load_integral() const {
    assert(holds_integral());
    return data;
}

void* integer_stack_storage::load_pointer() const {
    assert(holds_pointer());
    return as_pointer(data << 1);
}

bool integer_stack_storage::holds_integral() const {
    return check_if_two_most_significant_bits_are_the_same(data);
}

bool integer_stack_storage::holds_pointer() const {
    return !holds_integral();
}

void integer_stack_storage::mark_stored_value_as_pointer() {
    // sets most_significant_bit = !second_most_significant_bit
    data = data << 1 >> 1 | ~data >> (number_of_bits - 2) << (number_of_bits - 1);
}

} // namespace detail
} // namespace big